import React from 'react'
import {Link} from 'gatsby'

const DownContentF = () => {


    return(
       <React.Fragment>
            <div>
                <div className="DownContentF">
                    <div className="el">
                        <div className="el-icon">
                            <svg viewBox="0 0 25 28" fill="none" width="30"><path fill-rule="evenodd" clip-rule="evenodd" d="M23.27 13.629a.718.718 0 101.018-1.018l-1.62-1.622c.408-.61.631-1.334.631-2.069a3.724 3.724 0 00-3.72-3.72 3.724 3.724 0 00-3.72 3.72 3.724 3.724 0 003.72 3.72c.736 0 1.46-.223 2.07-.633l1.621 1.622zM17.3 8.92a2.282 2.282 0 012.28-2.28 2.282 2.282 0 012.28 2.28 2.283 2.283 0 01-2.28 2.28 2.283 2.283 0 01-2.28-2.28z" fill="#000"></path><path d="M3.619 12.4a2.162 2.162 0 01-2.16-2.16V8.8c0-1.191.969-2.16 2.16-2.16H13.7a.72.72 0 010 1.44H3.619a.72.72 0 00-.72.72v1.44c0 .397.323.72.72.72H13.7a.72.72 0 010 1.44H3.619zM3.979 16.72a1.08 1.08 0 100-2.16 1.08 1.08 0 000 2.16zM7.219 16.72a.72.72 0 010-1.44h11.52a.72.72 0 010 1.44H7.219zM3.979 21.76a1.08 1.08 0 100-2.16 1.08 1.08 0 000 2.16zM7.219 21.76a.72.72 0 010-1.44h11.52a.72.72 0 010 1.44H7.219zM3.979 26.8a1.08 1.08 0 100-2.16 1.08 1.08 0 000 2.16zM7.219 26.8a.72.72 0 010-1.44h11.52a.72.72 0 010 1.44H7.219z" fill="#000"></path></svg>
                        </div>
                        <div className="el-text">
                            <div className="el-title">
                            Ogromny wybór
                            </div>
                            <div className="el-subtitle">
                            Z łatwością porównasz oferty tysięcy lotów w jednym miejscu
                            </div>
                        </div>
                    </div>

                    <div className="el">
                        <div className="el-icon">
                        <svg viewBox="0 0 25 28" fill="none" width="30"><path d="M9.86 18.16a.72.72 0 01-.72-.72v-.339a2.86 2.86 0 01-1.637-1.055.715.715 0 01-.14-.533.715.715 0 01.712-.623c.227 0 .436.104.573.284.265.35.688.558 1.13.558h.053c.743 0 1.289-.406 1.289-.767 0-.36-.54-.765-1.261-.765-1.489 0-2.7-.99-2.7-2.206 0-1.01.816-1.865 1.98-2.125V9.52a.722.722 0 011.441 0v.34c.648.149 1.232.521 1.637 1.053a.72.72 0 11-1.146.872 1.43 1.43 0 00-1.13-.558l-.053.002c-.742 0-1.288.404-1.288.765 0 .362.539.766 1.26.766 1.489 0 2.7.989 2.7 2.205 0 1.01-.815 1.865-1.98 2.125v.35a.72.72 0 01-.72.72z" fill="#333"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M22.311 27.309a.72.72 0 001.019-1.018l-6.263-6.264a9.382 9.382 0 002.272-6.128c0-5.193-4.226-9.419-9.42-9.419C4.727 4.48.5 8.706.5 13.9c0 5.193 4.226 9.419 9.42 9.418a9.38 9.38 0 006.128-2.273l6.263 6.264zM1.94 13.899c0-4.4 3.58-7.979 7.98-7.979s7.979 3.58 7.979 7.98-3.58 7.979-7.98 7.979-7.979-3.58-7.979-7.98z" fill="#333"></path></svg>
                        </div>
                        <div className="el-text">
                            <div className="el-title"> 
                            Brak ukrytych opłat
                            </div>
                            <div className="el-subtitle">
                            Wiesz, za co płacisz
                            </div>
                        </div>
                    </div>

                    <div className="el">
                        <div className="el-icon">
                        <svg viewBox="0 0 25 28" fill="none" width="30"><path fill-rule="evenodd" clip-rule="evenodd" d="M6.583 27.52c-.265 0-.519-.062-.745-.181a1.575 1.575 0 01-.777-.932 1.574 1.574 0 01.11-1.207l1.48-3.081H4.563c-.992 0-1.886-.59-2.276-1.501l-1.65-3.726a1.6 1.6 0 011.465-2.235l1.867.084c.26.012.494.165.611.396l.684 1.358 2.4-.004-.646-1.301a1.603 1.603 0 01.302-1.842 1.585 1.585 0 011.128-.468H10.1c.503 0 .981.239 1.282.64l2.207 2.965 3.463-.005c1.591 0 2.881 1.277 2.889 2.846l-.002.082v.032a2.808 2.808 0 01-2.945 2.676l-3.588.001-3.912 4.821c-.309.369-.755.58-1.229.582H6.583zM2.08 16.098c-.065.004-.137.07-.137.157 0 .022.004.043.012.063l1.65 3.726c.167.387.542.636.957.636h3.234a.718.718 0 01.649 1.031l-1.987 4.135c-.036.069-.029.108-.021.132a.14.14 0 00.07.085c.02.011.047.017.077.017h1.679c.045 0 .089-.021.119-.058l4.12-5.08a.72.72 0 01.56-.266h3.965c.043.002.067.002.091.002.74 0 1.346-.574 1.381-1.307l.002-.057a1.418 1.418 0 00-1.42-1.396l-12.26.018a.716.716 0 01-.643-.396l-.693-1.38-1.405-.062zm6.257-1.732a.154.154 0 01.112-.046h1.65a.16.16 0 01.129.061l1.567 2.107-2.526.003-.961-1.942a.168.168 0 01-.017-.072c0-.043.017-.081.046-.11zM13.463 9.52c0 2.78 2.26 5.04 5.04 5.04 2.78 0 5.04-2.26 5.04-5.04 0-2.78-2.26-5.04-5.04-5.04-2.78 0-5.04 2.26-5.04 5.04zm1.44 0c0-1.985 1.615-3.6 3.6-3.6s3.6 1.615 3.6 3.6-1.615 3.6-3.6 3.6a3.604 3.604 0 01-3.6-3.6z" fill="#333"></path><path d="M18.503 10.24a.72.72 0 01-.72-.72V8.08a.72.72 0 011.44 0v.72h.72a.72.72 0 010 1.44h-1.44z" fill="#333"></path></svg>
                        </div>
                        <div className="el-text">
                            <div className="el-title">
                            Większa swoboda
                            </div>
                            <div className="el-subtitle">
                            Zachowaj swobodę wyboru terminu podróży dzięki możliwościom, jakie daje bilet elastyczny

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="subfoter">
                <div className="subfooter-list">
                    <div className="subfooter-list__el"><Link className='link' to="/">O Booking.com</Link></div>
                    <div className="subfooter-list__el"><Link className='link' to="/">Warunki</Link></div>
                    <div className="subfooter-list__el"><Link className='link'  to="/">Oświadczenie o ochronie prywatności i plikach cookie</Link></div>
                    <div className="subfooter-list__el"><Link className='link' to="/">Pomoc w zakresie lotów</Link></div>
                </div>
            </div>
       </React.Fragment>
    )
}
export default DownContentF

