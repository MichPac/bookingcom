import React,{useState,useRef,useEffect} from 'react'
import {Link} from 'gatsby'
import PopUp from '../../elements/FsearchFormEl/popUp'
import Input1 from '../../elements/FsearchFormEl/input1';
import Input2 from '../../elements/FsearchFormEl/input2';
import FCalendar  from '../../elements/FsearchFormEl/Calendar'
import Select from '../../elements/FsearchFormEl/autoWidthSelect'

const SearchFormF = () => {

    const[radioState,setRadioState]=useState('returnFlight')

    const[inputData,setInputData]= useState({
        from:'',
        where:'',
        date: new Date(),
        date2: '',
        class:"Ekonomiczna",
    })

    const handleChange = (e) => {
        const name = e.target.name
        setInputData({...inputData,[name]: e.target.value})
    }

    const [calendarPar,setCalendarPar] = useState({
        returnVal:'range',
        selectRange:undefined,
    })

    useEffect(()=>{
        if(radioState==='returnFlight'){
            setCalendarPar({
                returnVal:"range",
                selectRange:true,
            })
        }
        else if(radioState==='onewayFlight'){
            setInputData({
                ...inputData,
                date2:'',
            })
            setCalendarPar({
                returnVal:"start",
                selectRange:false,
            })
        }
        else{
            setCalendarPar({
                returnVal:"list",
                selectRange:false,
            })
        }
    },[radioState])


    return (
        <div>
            <div className="Flights">
                <div className="CovidBar">
                    <div className="covid-info">
                        Koronawirus (COVID-19) może wpłynąć na Twoje plany podróżnicze. <Link to="/">Dowiedz się więcej</Link>.
                    </div>
                </div>
                <div className="SearchFormF">
                    <div className="SearchFormF-title">
                        <div className="title">Wyszukaj i zarezerwuj swój kolejny lot</div>
                        <p className="subtitle">Gdy nadejdzie odpowiedni moment, będziemy tutaj</p>
                    </div>
                    <div>
                        <form>
                            <div  className="form-bar"> 
                                        <div className="radio-select">
                                                <label class="container1">
                                                    <input onClick={()=>setRadioState("returnFlight")} checked={calendarPar.returnVal==='range' ? 'true': ''}  name="radio" type="radio"/>
                                                    <span class="checkmark1"></span>
                                                    W obie strony
                                                </label>
                                                <label class="container1">
                                                    <input onClick={()=>setRadioState("onewayFlight")} type="radio" checked={calendarPar.returnVal==='start' ? 'true': ''} name="radio"/>
                                                    <span class="checkmark1"></span>
                                                    W jedną stronę
                                                </label>
                                                <label class="container1">
                                                    <input onClick={()=>setRadioState("list")}
                                                    checked={calendarPar.returnVal==='list' ? 'true': ''} type="radio" name="radio"  type="radio"/>
                                                    <span class="checkmark1"></span>
                                                    Kilka miast
                                                </label>
                                                
                                        </div>
                                        <div className="select-container">
                                            <div className="class">
                                                <Select
                                                    handleChange={handleChange}
                                                    inputData={inputData}
                                                />
                                                <div className="icon">
                                                    <svg viewBox="0 0 24 24" fill="#333333" width="24px" height="24px"><path d="M7 10.12l5 5 5-5H7z"></path></svg>
                                                </div>
                                            </div>
                                            <PopUp/>
                                        </div>
                            </div>
                            <div className="search-bar">
                                <Input1 
                                inputData={inputData}
                                setInputData={setInputData}/>
                                <Input2
                                inputData={inputData}
                                setInputData={setInputData}/>
                                <FCalendar
                                    calendarPar={calendarPar}
                                    inputData={inputData}
                                    setInputData={setInputData}
                                />
                                <div className="button">
                                    <div>Szukaj</div>
                                </div>
                            </div>
                            <div className="checkbox">
                                <label class="container">
                                    <input type="checkbox"/>
                                    <span class="checkmark"></span>
                                    Tylko loty bezpośrednie
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        
    )
}

export default  SearchFormF


// <select name="cars" id="cars" form="carform">
//                                         <option value="ekon">Ekonomiczna</option>
//                                         <option value="ekonP">Ekonomiczna premium</option>
//                                         <option value="biz">Biznesowa</option>
//                                         <option value="first">Pierwsza klasa</option>
//                                     </select>
//                                     <select name="cars" id="cars" form="carform">
//                                         <option value="ekon">Dorośli</option>
//                                         <option value="ekonP">Dzieci</option>
//                                     </select>