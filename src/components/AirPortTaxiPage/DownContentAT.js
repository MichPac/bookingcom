import React,{useState} from 'react'
import { StaticQuery, graphql} from "gatsby"



export default function DownContentAT() {

 const[visible,setVisible] = useState('1-3')

 const handleClick = (val) => {
    if(val==='1-3'){
        setVisible('1-3')
        console.log(visible)
    }
    else if(val==='4-7'){
        setVisible('4-7')
        console.log(visible)
    }
    else{
        setVisible('all')
        console.log(visible)
    }
    
 }

  return (
    <StaticQuery
      query={graphql`
        query{
            allAirPortTaxiJson {
                nodes {
                  luggages
                  class
                  car
                  id
                  passengers
                }
              }
          }
      `}
      render={data => (
        
        <React.Fragment>
                <div className="taxisForEveryoneAT">
                    <div className="taxisForEveryoneAT-container">
                        <div className="taxisForEveryoneAT-title">
                            <h2>Taksówki lotniskowe na każdy pobyt</h2>
                        </div>
                        <div className="taxisForEveryoneAT-bar">
                            <div className="taxisForEveryoneAT-bar__container">
                                <div className={visible==='1-3'? 'btn-container active' :'btn-container border-right'}>
                                    <button onClick={()=>handleClick('1-3')}>
                                        1-3 pasazerów
                                    </button>
                                </div>
                                <div className={visible==='4-7'? 'btn-container active' :'btn-container border-right'}>
                                    <button onClick={()=>handleClick('4-7')}>
                                        4-7 pasazerów
                                    </button>
                                </div>
                                <div className={visible==='all'? 'btn-container active' :'btn-container'}>
                                    <button onClick={()=>handleClick('all')}>
                                        Wszystkie taksówki
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="taxisForEveryoneAT-context">
                            <div className={visible=== '1-3' || visible=== 'all'  ? 'taxis' : 'taxis none'}>
                            {data.allAirPortTaxiJson.nodes.slice(0,2).map(el => (
                                <div index={el.id} className="taxi-container">
                                    <div className="taxi-title">
                                        <h4>{el.class}</h4>
                                        <p>{el.car} lub inne</p>
                                    </div>
                                    <div className="taxi-info">
                                        <div>   
                                            <svg width="24px" height="24px" class="rw-icon gb-o-interactive-field__icon" viewBox="0 0 128 128" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false"><path d="M104 120H24a8 8 0 0 1-8-8V93.5a4 4 0 0 1 1-2.7C21.3 86.4 36.9 72 64 72s42.8 14.4 47 18.8a4 4 0 0 1 1 2.7V112a8 8 0 0 1-8 8zM64 8a28 28 0 1 0 28 28A28 28 0 0 0 64 8z" fill-rule="evenodd"></path></svg>
                                            <span>{el.passengers} pasazerów</span>
                                        </div>
                                        <div>
                                            <svg class="bk-icon -iconset-suitcase" data-height="18" data-width="24" height="18" width="24" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M112 32H80v-4a12 12 0 0 0-12-12h-8a12 12 0 0 0-12 12v4H16a8 8 0 0 0-8 8v64a8 8 0 0 0 8 8h96a8 8 0 0 0 8-8V40a8 8 0 0 0-8-8zm-56-4a4 4 0 0 1 4-4h8a4 4 0 0 1 4 4v4H56zm-32 72a4 4 0 0 1-8 0V52a4 4 0 0 1 8 0zm88 0a4 4 0 0 1-8 0V52a4 4 0 0 1 8 0z"></path></svg>
                                            <span>{el.luggages} standardowych rozmiarów</span>
                                        </div>
                                        <div>
                                            <svg class="bk-icon -iconset-checkmark_unselected" data-height="18" data-width="24" fill="#0071C2" height="24" width="24" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M94.8 41.2a4 4 0 0 1 0 5.6l-40 40a4 4 0 0 1-5.6 0l-16-16a4 4 0 0 1 5.6-5.6L52 78.3l37.2-37.1a4 4 0 0 1 5.6 0zM120 64A56 56 0 1 1 64 8a56 56 0 0 1 56 56zm-8 0a48 48 0 1 0-48 48 48 48 0 0 0 48-48z"></path></svg>
                                            <span>Uwzględnione „Spotkanie z kierowcą”</span>
                                        </div>
                                        <div>
                                            <svg class="bk-icon -iconset-checkmark_bold" data-height="18" data-width="24" fill="#006607" height="24" width="24" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M52 102a8 8 0 0 1-5.7-2.3l-28-28a8 8 0 0 1 11.4-11.4L52 82.7l46.3-46.4a8 8 0 0 1 11.4 11.4l-52 52A8 8 0 0 1 52 102z"></path></svg>
                                            <span>Bezpłatne odwołanie</span>
                                        </div>
                                        <div className="search-btn">
                                            <button>Szukaj</button>
                                        </div>
                                    </div>
                                </div>
                            ))}
                            </div>
                            <div  className={visible==='4-7' || visible=== 'all' ? 'taxis' : 'taxis none'}>
                                {data.allAirPortTaxiJson.nodes.slice(2,5).map(el => (
                                    <div index={el.id} className="taxi-container2">
                                        <div className="taxi-title">
                                            <h4>{el.class}</h4>
                                            <p>{el.car} lub inne</p>
                                        </div>
                                        <div className="taxi-info">
                                            <div>   
                                                <svg width="24px" height="24px" class="rw-icon gb-o-interactive-field__icon" viewBox="0 0 128 128" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false"><path d="M104 120H24a8 8 0 0 1-8-8V93.5a4 4 0 0 1 1-2.7C21.3 86.4 36.9 72 64 72s42.8 14.4 47 18.8a4 4 0 0 1 1 2.7V112a8 8 0 0 1-8 8zM64 8a28 28 0 1 0 28 28A28 28 0 0 0 64 8z" fill-rule="evenodd"></path></svg>
                                                <span>{el.passengers} pasazerów</span>
                                            </div>
                                            <div>
                                                <svg class="bk-icon -iconset-suitcase" data-height="18" data-width="24" height="18" width="24" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M112 32H80v-4a12 12 0 0 0-12-12h-8a12 12 0 0 0-12 12v4H16a8 8 0 0 0-8 8v64a8 8 0 0 0 8 8h96a8 8 0 0 0 8-8V40a8 8 0 0 0-8-8zm-56-4a4 4 0 0 1 4-4h8a4 4 0 0 1 4 4v4H56zm-32 72a4 4 0 0 1-8 0V52a4 4 0 0 1 8 0zm88 0a4 4 0 0 1-8 0V52a4 4 0 0 1 8 0z"></path></svg>
                                                <span>{el.luggages} standardowych rozmiarów</span>
                                            </div>
                                            <div>
                                                <svg class="bk-icon -iconset-checkmark_unselected" data-height="18" data-width="24" fill="#0071C2" height="24" width="24" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M94.8 41.2a4 4 0 0 1 0 5.6l-40 40a4 4 0 0 1-5.6 0l-16-16a4 4 0 0 1 5.6-5.6L52 78.3l37.2-37.1a4 4 0 0 1 5.6 0zM120 64A56 56 0 1 1 64 8a56 56 0 0 1 56 56zm-8 0a48 48 0 1 0-48 48 48 48 0 0 0 48-48z"></path></svg>
                                                <span>Uwzględnione „Spotkanie z kierowcą”</span>
                                            </div>
                                            <div>
                                                <svg class="bk-icon -iconset-checkmark_bold" data-height="18" data-width="24" fill="#006607" height="24" width="24" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M52 102a8 8 0 0 1-5.7-2.3l-28-28a8 8 0 0 1 11.4-11.4L52 82.7l46.3-46.4a8 8 0 0 1 11.4 11.4l-52 52A8 8 0 0 1 52 102z"></path></svg>
                                                <span>Bezpłatne odwołanie</span>
                                            </div>
                                            <div className="search-btn">
                                                <button>Szukaj</button>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
        </React.Fragment>
      )}
    />
  )
}

