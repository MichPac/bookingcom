import React from 'react'
import { StaticQuery, graphql,Link } from "gatsby"
import AccordionAT from '../../elements/accordion/AccordionAT';



export default function ReadmoreAT() {
  return (
    <StaticQuery
      query={graphql`
        query{
            allAccordionAtJson {
                nodes {
                  id
                  question
                  answer
                }
              }
          }
      `}
      render={data => (
        <React.Fragment>
                <div className="readMoreAT">
                                <div className="readMoreAT-container">
                                    <div className="findMore">
                                            <div className="findMore-title">
                                                <h2>Dowiedz się więcej na temat usługi taksówki lotniskowej</h2>
                                            </div>
                                            <div className="findMore-desc">
                                                <p>Zobacz więcej często zadawanych pytań w naszym</p>
                                            </div>
                                            <div className="findMore-link"><Link className="Link" to="/">centrum pomocy</Link></div>
                                    </div>
                                    <div className="questions">
                                    {data.allAccordionAtJson.nodes.map(el => (
                                        <AccordionAT {...el}/>
                                    ))}
                                    </div>
                                </div>
                </div>
        </React.Fragment>
      )}
    />
  )
}

