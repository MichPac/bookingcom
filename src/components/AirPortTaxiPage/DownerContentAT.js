import React from 'react'

function DownerContentAT() {
    
    return (
        <div className="downerContentAT">
                  <div className="downerContentAT-info__container">
                      <div className="downerContentAT-info">
                        <div className="icon g">
                        <span aria-hidden="true" role="presentation" class="cf18357eee _00bca8e4ac ae35e0ff09 edb50b1eb4"><svg viewBox="0 0 128 128" width="2em" height="2em" role="img" aria-hidden="true"><path d="M8.3 83.1l2.8-2.8a1 1 0 0 1 .7-.3h27.3l16-17.5-41.7-32a4 4 0 0 1-1.1-5.3l1.3-2.8a4 4 0 0 1 5.1-1.6l55.5 21.1L98 16a28.6 28.6 0 0 1 18-8 4 4 0 0 1 4 4 28.6 28.6 0 0 1-8 18L86.6 53.4l21 55.3a4 4 0 0 1-1.6 5.1l-2.7 1.4A4 4 0 0 1 98 114L66 72.3 48 89v27.3a1 1 0 0 1-.3.7l-2.8 2.8a1 1 0 0 1-1.6-.2L30.7 97.3 8.5 84.7a1 1 0 0 1-.2-1.6z"></path></svg></span>
                        </div>
                        <div className="desc">
                          <div className="desc-title">Monitorowanie lotu</div>
                          <div className="desc-desc">Twój kierowca monitoruje Twój lot i zaczeka na Ciebie, jeśli lot się opóźni</div>
                        </div>
                      </div>
                      <div className="downerContentAT-info">
                        <div className="icon o">
                        <span aria-hidden="true" role="presentation" class="cf18357eee _00bca8e4ac ae35e0ff09 edb50b1eb4"><svg viewBox="0 0 128 128" width="2em" height="2em" role="img" aria-hidden="true"><path d="M108 24H20A12 12 0 0 0 8 36v56a12 12 0 0 0 12 12h88a12 12 0 0 0 12-12V36a12 12 0 0 0-12-12zm-88 8h88a4 4 0 0 1 4 4v4H16v-4a4 4 0 0 1 4-4zm88 64H20a4 4 0 0 1-4-4V56h96v36a4 4 0 0 1-4 4zM24 72h48v8H24z"></path></svg></span>
                        </div>
                        <div className="desc">
                          <div className="desc-title">Monitorowanie lotu</div>
                          <div className="desc-desc">Cena zostaje ustalona z góry – brak dodatkowych kosztów, gotówka nie jest wymagana </div>
                        </div>
                      </div>
                      <div className="downerContentAT-info">
                        <div className="icon b">
                        <span aria-hidden="true" role="presentation" class="cf18357eee _00bca8e4ac ae35e0ff09 edb50b1eb4"><svg viewBox="0 0 128 128" width="2em" height="2em" role="img" aria-hidden="true"><path d="M52 102a8 8 0 0 1-5.7-2.3l-28-28a8 8 0 0 1 11.4-11.4L52 82.7l46.3-46.4a8 8 0 0 1 11.4 11.4l-52 52A8 8 0 0 1 52 102z"></path></svg></span>
                        </div>
                        <div className="desc">
                          <div className="desc-title">Monitorowanie lotu</div>
                          <div className="desc-desc">Współpracujemy z profesjonalnymi kierowcami oraz oferujemy całodobowe wsparcie</div>
                        </div>
                      </div>
                  </div>
              </div>
    )
}

export default DownerContentAT
