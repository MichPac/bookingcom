import React,{useState} from 'react'
import PopUp from '../../elements/ATsearchFormEl/PopUp'
import ATCalendar from '../../elements/ATsearchFormEl/Calendar';



const SearchBarAT = () => {

    const[radio,setRadio] = useState('oneS')

    const [inputData,setInputData] = useState({
        pickupPlace:'',
        dropPlace:'',
        date: new Date(),
        date2: new Date(),
        h:'12',
        m:'00',
        h1:'12',
        m2:'00',
        people:1,
    })

    const handleChange = (e) => {
        const name = e.target.name
        setInputData({...inputData,[name]: e.target.value})
    }



        return(
            <div className="SearchBarAT-wrapper">
                <div className="SearchBarAT-title">
                    <div className="title">Zarezerwuj taksówkę lotniskową</div>
                    <div className="subtitle">Łatwy transport taksówką lotniskową do i z obiektu</div>
                </div>
                <div className="SearchBarAT-bar">
                    <form>
                        <div className="radio-btns">
                                <label class="containerR"> 
                                    <input  onClick={()=>setRadio('oneS')} type="radio" checked={radio==='oneS' ? true : false} name="radioAT"/>
                                    <span class="checkmarkR"></span>
                                    W jedną stronę
                                </label>
                                <label class="containerR2">
                                    <input onClick={()=>setRadio('bothS')} checked={radio==='bothS' ? true : false} type="radio" name="radioAT"/>
                                    <span class="checkmarkR2"></span>
                                    W obie strony
                                </label>
                            </div>
                            <div className="search-bar">
                                <div className="border wrapper">
                                    <label className="border place">
                                        <div className="place-wrapper">
                                            <span>  
                                            <svg fill="grey" class="rw-icon rw-icon gb-o-interactive-field__icon" viewBox="0 0 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false" width="24px" height="24px"><path d="M31.939 29.918c4.816 0 8.734-3.918 8.734-8.734 0-4.817-3.918-8.735-8.734-8.653-4.817 0-8.653 3.918-8.653 8.734-.082 4.735 3.836 8.653 8.653 8.653M30.796 2h2.367c.898.163 1.878.245 2.776.408 3.51.735 6.612 2.368 9.143 4.898 2.938 2.857 4.734 6.286 5.469 10.286.163.898.245 1.796.327 2.694v1.306c-.082.735-.164 1.47-.327 2.286-.653 3.918-1.96 7.673-3.51 11.346-3.919 9.143-8.898 17.796-14.286 26.123-.163.245-.408.408-.571.653h-.49c-.163-.245-.327-.408-.49-.653-5.633-7.918-10.612-16.082-14.367-25.061-1.388-3.347-2.613-6.857-3.266-10.53-.244-1.389-.408-2.776-.571-4.164v-1.388c0-.163.082-.326.082-.49.081-2.612.816-5.143 1.959-7.428 2.53-4.898 6.449-8.164 11.755-9.633 1.388-.326 2.694-.49 4-.653" fill-rule="evenodd"></path></svg>
                                            </span>
                                            <input
                                                name="pickupPlace"
                                                onChange={(e)=>handleChange(e)} 
                                                value={inputData.pickupPlace} 
                                                placeholder="Wpisz miejsce odbioru"
                                            />
                                        </div>
                                    </label>
                                    <label className="place">
                                        <div className="place-wrapper">
                                            <span>  
                                            <svg fill="grey" class="rw-icon rw-icon gb-o-interactive-field__icon" viewBox="0 0 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false" width="24px" height="24px"><path d="M31.939 29.918c4.816 0 8.734-3.918 8.734-8.734 0-4.817-3.918-8.735-8.734-8.653-4.817 0-8.653 3.918-8.653 8.734-.082 4.735 3.836 8.653 8.653 8.653M30.796 2h2.367c.898.163 1.878.245 2.776.408 3.51.735 6.612 2.368 9.143 4.898 2.938 2.857 4.734 6.286 5.469 10.286.163.898.245 1.796.327 2.694v1.306c-.082.735-.164 1.47-.327 2.286-.653 3.918-1.96 7.673-3.51 11.346-3.919 9.143-8.898 17.796-14.286 26.123-.163.245-.408.408-.571.653h-.49c-.163-.245-.327-.408-.49-.653-5.633-7.918-10.612-16.082-14.367-25.061-1.388-3.347-2.613-6.857-3.266-10.53-.244-1.389-.408-2.776-.571-4.164v-1.388c0-.163.082-.326.082-.49.081-2.612.816-5.143 1.959-7.428 2.53-4.898 6.449-8.164 11.755-9.633 1.388-.326 2.694-.49 4-.653" fill-rule="evenodd"></path></svg>
                                            </span>
                                            <input 
                                                name="dropPlace"
                                                onChange={(e)=>handleChange(e)} 
                                                value={inputData.dropPlace} 
                                                placeholder="Wpisz miejsce docelowe"
                                            />
                                        </div>
                                    </label>
                                </div>
                                <div className="wrapper2">
                                    <ATCalendar
                                        setInputData={setInputData}
                                        inputData={inputData}
                                    />
                                    <PopUp
                                        handleChange={handleChange}
                                        inputData={inputData}
                                    />
                                    <div className="people">
                                        <div className="people-num">
                                            <span className="person-icon"><svg width="24px" height="24px" fill="grey" class="rw-icon gb-o-interactive-field__icon" viewBox="0 0 128 128" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false"><path d="M104 120H24a8 8 0 0 1-8-8V93.5a4 4 0 0 1 1-2.7C21.3 86.4 36.9 72 64 72s42.8 14.4 47 18.8a4 4 0 0 1 1 2.7V112a8 8 0 0 1-8 8zM64 8a28 28 0 1 0 28 28A28 28 0 0 0 64 8z" fill-rule="evenodd"></path></svg></span>
                                            <select>
                                                        <option disabled="true">Pasazerowie</option>
                                                        <option value={1}>1</option>
                                                        <option value={2}>2</option>
                                                        <option value={3}>3</option>
                                                        <option value={4}>4</option>
                                                        <option value={5}>5</option>
                                                        <option value={6}>6</option>
                                                        <option value={7}>7</option>
                                                        <option value={8}>8</option>
                                                        <option value={9}>9</option>
                                                        <option value={10}>10</option>
                                                        <option value={11}>11</option>
                                                        <option value={12}>12</option>
                                                        <option value={13}>13</option>
                                                        <option value={14}>14</option>
                                                        <option value={15}>15</option>
                                                        <option value={16}>16</option>
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <button className="button">
                                <div>Szukaj</div>
                            </button>
                            </div>
                    </form>
                </div>
            </div>
        )
}

export default SearchBarAT


