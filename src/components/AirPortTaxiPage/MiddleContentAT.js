import React from 'react'

function MiddleContentAT() {
    return (
        <div className="middleContentAT">
                <div className="middleContentAT-title">
                  <h1>Prosty transfer lotniskowy</h1>
                </div>
                <div className="middleContnetAT-container">
                 
                  <div className="middleContentAT-info">
                        <div className="middleContentAT-info__single">
                          <div className='img'>
                            <img src="https://cf.bstatic.com/static/img/taxi-book-taxi/72b2754b7952cf88eb5752336a0b28aa108c192b.svg" alt="Booking Airport Taxi"></img>
                          </div>
                          <div className="middleContentAT-info__container">
                            <div className="middleContentAT-info__title">Rezerwowanie taksówki lotniskowej</div>
                              <div className="middleContentAT-info__desc">Potwierdzenie jest natychmiastowe. W przypadku zmiany planów możesz bezpłatnie odwołać rezerwację do 24 godzin przed zaplanowaną godziną odbioru</div>
                              </div>
                        </div>

                        <div className="middleContentAT-info__single">
                          <div className='img'>
                            <img src="https://cf.bstatic.com/static/img/taxi-meet-driver/80b8363402262d17552ebd80ecd51b6ddbe3c7f6.svg" alt="Booking Airport Taxi"/>
                          </div>
                          <div className="middleContentAT-info__container">
                          <div className="middleContentAT-info__title">Spotkanie z kierowcą</div>
                            <div className="middleContentAT-info__desc">Kierowca spotka się z Tobą w hali przylotów, a następnie zaprowadzi Cię do samochodu. Kierowca będzie monitorował Twój lot i zaczeka, jeśli będzie on opóźniony</div>
                            </div>
                        </div>

                        <div className="middleContentAT-info__single">
                          <div className='img'>
                            <img src="https://cf.bstatic.com/static/img/taxi-arrive-at-destination/75cc4d191f8e30821c73817602a4e160955fe653.svg" alt="Booking Airport Taxi"/>
                          </div>
                          <div className="middleContentAT-info__container">
                          <div className="middleContentAT-info__title">Przyjazd na miejsce docelowe</div>
                            <div className="middleContentAT-info__desc">Bezpiecznie i szybko dotrzyj do celu – brak oczekiwania na taksówkę lub szukania odpowiedniego środka transportu publicznego</div>
                            </div>
                        </div>
                      <div>
                    </div> 
                  </div>
                  <div className="middleContentAT-img">
                    <img src="https://cf.bstatic.com/static/img/taxi-how-it-works/79d898d0e29dd469799e76b6d4bb71b5b0f03707.svg" class="landing-page__section-taxi-how-it-works-img " alt="Booking Airport Taxi"></img>
                    </div> 
                  <div className="middleContentAT-howWorks">
                  </div>
                </div>
              </div>

    )
}

export default MiddleContentAT