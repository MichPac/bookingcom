import React from 'react'
import { StaticQuery, graphql} from "gatsby"


export default function UpContentA() {

  
  return (
    <StaticQuery
      query={graphql`
        query{
            allAttractionsJson {
                nodes {
                  id
                  attractions
                  city
                  imgPath
                }
              }
          }
      `}
      render={data => (
        
        <div className="UpContentA">
            <div className="UpContentA-title">
                <h2>Najpopularniejsze lokalizacje</h2>
            </div>
            <div className="UpContentA-list">
            {data.allAttractionsJson.nodes.map(el => (
                <div index={el.id} className="UpContentA-list__el el">
                <div className="img-wrapper">
                    <img className="el-img" src={el.imgPath}/>
                    <div className="shadow"></div>
                </div>
                    <div className="desc-wrapper">
                        <div className="desc"> 
                            <div className="city">{el.city}</div>
                            <div className="attractions">{el.attractions} atrakcji</div>
                        </div>
                    </div>
                </div>
                
            ))}
                
            </div>
        </div>
      )}
    />
  )
}

