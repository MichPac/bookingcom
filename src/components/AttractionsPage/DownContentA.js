import React from 'react'
import { StaticQuery, graphql} from "gatsby"


export default function DownContentA() {

  
  return (
    <StaticQuery
      query={graphql`
        query{
            allAttractions2Json {
                nodes {
                  attractions
                  region
                  city
                  id
                  imgPath
                }
              }
          }
      `}
      render={data => (
        
         <div className="DownContentA">
            <div className="DownContentA-title">
                <h3>Odkryj więcej miejsc</h3>
                <h4>Znajdź atrakcje w miastach na całym świecie</h4>
            </div>
            <div className="DownContentA-bar">
                <div className="DownContentA-bar__list">
                    <div><button>Europa</button></div>
                    <div><button>Ameryka Północna</button></div>
                    <div><button>Azja</button></div>
                    <div><button>Afryka</button></div>
                    <div><button>Oceania</button></div>
                    <div><button>Bliski Wschód</button></div>
                    <div><button>Karaiby</button></div>
                    <div><button>Ameryka Południowa</button></div>
                    <div><button>Ameryka Środkowa</button></div>
                </div> 
            </div>
            <div className="DownContentA-list">
                {data.allAttractions2Json.nodes.map(el => (
                    <div index={el.id} className="DownContentA-list__el">
                    <div className="img-wrapper">
                        <img className="el-img" src={el.imgPath}/>
                        <div className="shadow"></div>
                    </div>
                        <div className="desc-wrapper">
                            <div className="desc"> 
                                <div className="city">{el.city}</div>
                                <div className="attractions">{el.attractions} atrakcji</div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
      )}
    />
  )
}

