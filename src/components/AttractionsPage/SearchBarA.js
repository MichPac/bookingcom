import React from 'react'

const SearchBarA = () => {


    return(
        <div className="SearchBarA">
            <div className="SearchBarA-wrapper">
                <div className="SearchBarA-title">
                    <div className="title">Znajdź i zarezerwuj niezapomniane doświadczenie</div>
                    <div className="sub-title">Odkryj miejsce, do którego się wybierasz, i jak najlepiej wykorzystaj swoją podróż</div>
                </div>
                <div className="SearchBarA-bar">
                    <form className="form">
                        <label>
                            <span>  
                                <svg width="26" height="26" viewBox="0 0 24 24" focusable="false" role="img" aria-hidden="true"><path d="M17.464 6.56a8.313 8.313 0 1 1-15.302 6.504A8.313 8.313 0 0 1 17.464 6.56zm1.38-.586C16.724.986 10.963-1.339 5.974.781.988 2.9-1.337 8.662.783 13.65c2.12 4.987 7.881 7.312 12.87 5.192 4.987-2.12 7.312-7.881 5.192-12.87zM15.691 16.75l7.029 7.03a.75.75 0 0 0 1.06-1.06l-7.029-7.03a.75.75 0 0 0-1.06 1.06z"></path></svg>
                            </span>
                            <input type="text" placeholder="Ciekawe miejsca, muzea, wycieczki..."></input>
                        </label>
                        <button>Szukaj</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default SearchBarA

