import React,{useEffect, useState} from "react"
import { useStaticQuery, graphql } from "gatsby"


const DownContentA2 = () => {
  const data = useStaticQuery(graphql`
  query{
    allAttractions2Json {
        nodes {
        region
          attractions
          city
          id
          imgPath
        }
      }
  }
  `)
  const listA = data.allAttractions2Json.nodes
  const [updatedListA, setUpdatedListA] = useState(listA)
  const[active, setActive] = useState('Europa')
  const Arr = ['Europa','Ameryka Pn','Azja','Afryka','Oceania','Bliski Wschód','Karaiby','Afryka Południowa','Ameryka Środkowa']
  
  useEffect(()=>{
    const filteredList = [...listA].filter(el => el.region == active)
    setUpdatedListA(filteredList)
  },[active])


  return (
      
    <div className="DownContentA">
            <div className="DownContentA-title">
                <h3>Odkryj więcej miejsc</h3>
                <h4>Znajdź atrakcje w miastach na całym świecie</h4>
            </div>
            <div className="DownContentA-bar">
                <div className="DownContentA-bar__list">
                    {
                        
                        Arr.map((el,index) => (
                            <div>
                                <button className={active == el ? "active" : " "} index={index} onClick={()=>setActive(el)}>
                                {el}
                                </button>
                            </div>
                        ))
                    }
                </div> 
            </div>
            <div className="DownContentA-list">
                {
                    updatedListA.map((el,index) =>{
                    return (
                    <div key={el.id} index={index} className="DownContentA-list__el el">
                        <div className="img-wrapper">
                            <img className="el-img" src={el.imgPath}/>
                            <div className="shadow"></div>
                        </div>
                            <div className="desc-wrapper">
                                <div className="desc"> 
                                    <div className="city">{el.city}</div>
                                    <div className="attractions">{el.attractions} atrakcji</div>
                                </div>
                            </div>
                        </div>
                    )})
                }
            </div>
        </div>
  )
}
export default DownContentA2


