import React from 'react'

const CovidInfo = () => {
    return (
        <div className="Search-section">
            <div className="Search-section__covidInfo">
                <div className="Search-sectionCovidInfo__title">
                    <div className="sectionCovidInfo__icon">
                        <svg fill="#964B00" class="bk-icon -streamline-info_sign"  height="30" width="30" viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false"><path d="M14.25 15.75h-.75a.75.75 0 0 1-.75-.75v-3.75a1.5 1.5 0 0 0-1.5-1.5h-.75a.75.75 0 0 0 0 1.5h.75V15a2.25 2.25 0 0 0 2.25 2.25h.75a.75.75 0 0 0 0-1.5zM11.625 6a1.125 1.125 0 1 0 0 2.25 1.125 1.125 0 0 0 0-2.25.75.75 0 0 0 0 1.5.375.375 0 1 1 0-.75.375.375 0 0 1 0 .75.75.75 0 0 0 0-1.5zM22.5 12c0 5.799-4.701 10.5-10.5 10.5S1.5 17.799 1.5 12 6.201 1.5 12 1.5 22.5 6.201 22.5 12zm1.5 0c0-6.627-5.373-12-12-12S0 5.373 0 12s5.373 12 12 12 12-5.373 12-12z"></path></svg>
                    </div>
                    <div className="sectionCovidInfo__title">
                        <h4>Coronavirus (COVID-19) support</h4>
                    </div>
                    <div className="sectionCovidInfo__rollUp">
                    <svg class="bk-icon -streamline-arrow_nav_up" height="24" width="24" viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false"><path d="M6 14.55a.74.74 0 0 1 .22-.55l5-5c.21-.2.49-.309.78-.3a1.1 1.1 0 0 1 .78.32l5 5a.75.75 0 0 1 0 1.06.74.74 0 0 1-1.06 0L12 10.36l-4.72 4.72a.74.74 0 0 1-1.06 0 .73.73 0 0 1-.22-.53zm5.72-4.47zm.57 0z"></path></svg>
                    </div>
                </div>
                <div className="Search-section__desc">
                    <span>Check for travel restrictions. Travel might only be permitted for certain purposes, and touristic travel in particular may not be allowed.</span>
                </div>
                <div className="Search-section__readMore">
                    <a href="#essa">Read More</a>
                </div>
            </div>

           
        </div>
    )
}

export default CovidInfo


