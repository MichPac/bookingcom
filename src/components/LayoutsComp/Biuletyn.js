import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


const  Biuletyn = () => {

  return (
        
        <div className="Biuletyn">
          <div className="Biuletyn-title">
                <h2>Oszczędzaj czas i pieniądze!</h2>
          </div>
          <div className="Biuletyn-form">
                <div className="Biuetyn-text">
                    Zaprenumeruj nasz biuletyn, a będziemy przesyłać Ci najlepsze oferty
                </div>
                <div className="form">
                    <form>
                        <input className="mail form-input" type="email" placeholder="Adres mailowy" />
                        <button className="form-button">Zaprenumeruj</button>
                        <div className="checkbox-container">
                        <input id="biuletynCh" type="checkbox"/><label for="biuletynCh">Wyślij mi link do pobrania BEZPŁATNEJ aplikacji Booking.com!</label>
                        </div>
                    </form>
                </div>
          </div>
        </div>
    )
}

export default Biuletyn
