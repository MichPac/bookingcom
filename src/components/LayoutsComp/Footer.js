import React from 'react';


const Footer = () => {
    return (
        <div>
            
            <div className="footer">
                <div className="copyRights">
                    Prawa autorskie © 1996–2021 Booking.com™. Wszystkie prawa zastrzeżone.
                </div>
                <div className="footer-text">
                    Booking.com jest częścią Booking Holdings Inc. – światowego lidera w internetowej branży turystycznej.
                </div>
                <div className="logos">
                <img src="https://q-xx.bstatic.com/static/img/tfl/group_logos/logo_booking/27c8d1832de6a3123b6ee45b59ae2f81b0d9d0d0.png" alt="Booking.com" title="Booking.com" class="css-q7p1qv"/>
                <img src="https://q-xx.bstatic.com/static/img/tfl/group_logos/logo_priceline/f80e129541f2a952d470df2447373390f3dd4e44.png" alt="Priceline.com" title="Priceline.com" class="css-q7p1qv"/>
                <img src="https://q-xx.bstatic.com/static/img/tfl/group_logos/logo_kayak/83ef7122074473a6566094e957ff834badb58ce6.png" alt="Kayak" title="Kayak" class="css-q7p1qv"/>
                <img src="https://q-xx.bstatic.com/static/img/tfl/group_logos/logo_agoda/3d4f6ca8a45a376f2193f1e88d1ac8369f585e76.png" alt="Agoda" title="Agoda" class="css-q7p1qv"/>
                <img src="https://q-xx.bstatic.com/static/img/tfl/group_logos/logo_rentalcars/6bc5ec89d870111592a378bbe7a2086f0b01abc4.png" alt="Rentalcars" title="Rentalcars" class="css-q7p1qv"/>
                <img src="https://q-xx.bstatic.com/static/img/tfl/group_logos/logo_opentable/a4b50503eda6c15773d6e61c238230eb42fb050d.png" alt="OpenTable" title="OpenTable" class="css-q7p1qv"/>
                </div>
            </div>
        </div>
    )
}

export default Footer

