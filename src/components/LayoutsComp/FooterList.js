import React from 'react'

const FooterList = () => {
    return (
        <React.Fragment>
        <div className="FooterList">
            <div className="list-contanier">
                <ul className="list">
                    <li>Kraje</li>
                    <li>Regiony</li>
                    <li>Miasta</li>
                    <li>Dzielnice</li>
                    <li>Lotniska</li>
                    <li>Hotele</li>
                    <li>Ciekawe miejsca</li>
                </ul>
            </div>   
            <div className="list-contanier">
                <ul className="list">
                    <li>Domy</li>
                    <li>Apartamenty</li>
                    <li>Ośrodki wypoczynkowe</li>
                    <li>Wille</li>
                    <li>Hostele</li>
                    <li>Obiekty B&B</li>
                    <li>Pensjonaty</li>
                </ul>
            </div>  
            <div className="list-contanier">
                <ul className="list">
                    <li>Wyjątkowe miejsca na pobyt</li>
                    <li>Wszystkie cele podrózy</li>
                    <li>Opinie</li>
                    <li>Artykuły</li>
                    <li>Społeczności podrózujących</li>
                    <li>Oferty sezonowe i wakacyjne</li>
                </ul>
            </div>
            <div className="list-contanier">
                <ul className="list">
                    <li>Wypożyczalnia samochodów</li>
                    <li>Wyszukiwarka lotów</li>
                    <li>Rezerwacja restauracji</li>
                    <li>Booking.com dla Biur Podróży</li>
                </ul>
            </div>
            <div className="list-contanier">
                <ul className="list">
                    <li>Często zadawane pytania dotyczące koronawirusa (COVID-19)</li>
                    <li>O Booking.com</li>
                    <li>Skontaktuj się z obsługą klienta</li>
                    <li>Centrum pomocy</li>
                    <li>Careers </li>
                    <li>Zrównoważony rozwój</li>
                    <li>Informacje prasowe</li>
                    <li>Relacje z inwestorami </li>
                    <li>Ogólne Warunki Handlowe </li>
                    <li>Rozstrzyganie sporów</li>
                    <li>Zasady współpracy</li>
                    <li>Oświadczenie o ochronie prywatności i plikach cookies</li>
                    <li>Zarządzaj ustawieniami dotyczącymi plików cookie</li>
                    <li>Kontakt dla firm</li>
                </ul>
            </div>
        </div>
        <div className="log">
            <div className="log-btn">Logowanie do Extranetu</div>
        </div>
        </React.Fragment>
    )
}

export default FooterList
