/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

 import * as React from "react"
 import Header from '../components/LayoutsComp/Header2'
 import Footer from "../components/LayoutsComp/Footer"
 import "./layout.css"
 
 const Layout1 = (props) => {

 
   return (
     <>
       <Header/>
            {props.children}
       <Footer/>
     </>
   )
 }

 export default Layout1