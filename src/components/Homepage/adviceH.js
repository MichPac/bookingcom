import React from 'react'
import { useStaticQuery, graphql,Link } from "gatsby";


const AdviceH = () => {
    


    return(
        <div className="Hadvice">
            <div className="Hadivce-icon__wrapper">
                <div className="icon-background">
                    <svg class="bk-icon -streamline-info_sign" fill="#923e01" height="30" width="30" viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false"><path d="M14.25 15.75h-.75a.75.75 0 0 1-.75-.75v-3.75a1.5 1.5 0 0 0-1.5-1.5h-.75a.75.75 0 0 0 0 1.5h.75V15a2.25 2.25 0 0 0 2.25 2.25h.75a.75.75 0 0 0 0-1.5zM11.625 6a1.125 1.125 0 1 0 0 2.25 1.125 1.125 0 0 0 0-2.25.75.75 0 0 0 0 1.5.375.375 0 1 1 0-.75.375.375 0 0 1 0 .75.75.75 0 0 0 0-1.5zM22.5 12c0 5.799-4.701 10.5-10.5 10.5S1.5 17.799 1.5 12 6.201 1.5 12 1.5 22.5 6.201 22.5 12zm1.5 0c0-6.627-5.373-12-12-12S0 5.373 0 12s5.373 12 12 12 12-5.373 12-12z"></path></svg>
                </div>
                <div className="Hadivce-icon__text">
                    <div className="text">Otrzymaj porady, których potrzebujesz. Sprawdź najnowsze ograniczenia związane z COVID-19, zanim wyruszysz w podróż.
                    <Link> Dowiedz się więcej</Link></div>
                </div>
            </div>
        </div>
    )
}

export default AdviceH