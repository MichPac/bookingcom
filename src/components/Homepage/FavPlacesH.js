import React,{useState} from 'react'
import { useStaticQuery, graphql} from "gatsby";


const FavPlacesH = () => {


  const[listNum,setListNum] = useState({f:0,s:20})
  const[active,setActive] = useState('regions')
  
  const handleClick = (f,s,val) => {
    setListNum({f:f,s:s})
    setActive(val)
  }

  const data = useStaticQuery(graphql`
    query{
            allFavPlacesJson {
                nodes {
                  id
                  name
                  objects
                }
              }
          }
  `)

  return(
      
     <div className="FavPlacesH">
            <div className="FavPlacesH-title">
                <h3>Nasze ulubione miejsca</h3>
            </div>
            <div className="FavPlacesH-bar">
                <div className="FavPlacesH-bar__list">
                    <div>
                      <button onClick={()=>handleClick(0,20,'regions')} className={active==='regions' ? "active" : ""}>Regiony
                      </button>
                    </div>
                    <div>
                      <button onClick={()=>handleClick(20,80,'cities')} className={active==='cities' ? "active" : ""}>Miasta
                      </button>
                    </div>
                    <div>
                      <button onClick={()=>handleClick(80,100,'iplaces')} className={active==='iplaces' ? "active" : ""}>Ciekawe miejsca
                      </button>
                    </div>
                </div>
            </div>
            <div className="FavPlacesH-list">
                {data.allFavPlacesJson.nodes.slice(listNum.f,listNum.s).map(el => (
                <div index={el.index} className="FavPlacesH-list__el">
                    <div className="el-name">{el.name}</div>
                    <div className="el-objects">{el.objects}</div>
                </div>
                ))}
            </div>
        </div>
    )
  
}

export default FavPlacesH






