import React from 'react'

function InspirationComp() {
    return (
        <React.Fragment>
            <div className="mainContainerIO">
            <h2>Poszukaj inspiracji na kolejną podróź</h2>
                <div className="upperContentIO">

                    <div className="upperContentIO-pic1">
                        <div className="pic1"></div>
                        <div className="IO-desc">
                            <div className="Io-desc__city">
                                <div className="Io-desc__cityUp">
                                    <p>10 ciekawostek z krajów na całym świecie</p>
                                </div>
                                <span>Dodaj te miejsca do swojej listy życzeń i zobacz je na własne oczy, gdy przyjdzie czas.</span>
                            </div>
                        </div>
                    </div>
                    <div className="upperContentIO-pic1">
                    <div className="pic2"></div>
                        <div className="IO-desc">
                            <div className="Io-desc__city">
                                <div className="Io-desc__cityUp">
                                    <p>Najlepsze domy wakacyjne na Santorynie</p>
                                </div>
                                <span>Spędź długie letnie dni w tych olśniewających domach wakacyjnych na Santorynie.</span>
                            </div>
                        </div>
                    </div>
                    <div className="upperContentIO-pic1">
                    <div className="pic3"></div>
                        <div className="IO-desc">
                            <div className="Io-desc__city">
                                <div className="Io-desc__cityUp">
                                    <p>Lokalny przewodnik po Portugalii</p>
                                </div>
                                <span>Podążaj śladami lokalnych podróżujących i odkryj Portugalię, której jeszcze nie widziałeś.</span>
                            </div>
                    </div>
            </div>
            </div>
                <div className="upperContentIO">
                    <div className="upperContentIO-pic1">
                    <div className="pic4"></div>
                        <div className="IO-desc">
                            <div className="Io-desc__city">
                                <div className="Io-desc__cityUp">
                                    <p>Wspaniałe podróże pociągiem: Ekspres Lodowcowy</p>
                                    
                                </div>
                                <span>Daj się oczarować magicznym krajobrazom Szwajcarii.</span>
                            </div>
                        </div>
                    </div>
                    <div className="upperContentIO-pic1">
                    <div className="pic5"></div>
                        <div className="IO-desc">
                            <div className="Io-desc__city">
                                <div className="Io-desc__cityUp">
                                    <p>Te niezwykłe miejsca muszą być na Twojej liście!</p>
                                </div>
                                <span>Stwórz podróżniczą listę życzeń z miejscami, które już niedługo będziesz mógł zobaczyć.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )

}

export default InspirationComp
