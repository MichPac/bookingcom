import React from "react"
import { useStaticQuery, graphql,Link } from "gatsby"
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { ArrowForwardIos } from '@material-ui/icons';


const CitiesH = () => {

    const PrevArrow = props => {
        const { className, onClick } = props;
        return (
            <div className={className} onClick={onClick}>
                <ArrowForwardIos style={{height:'20px', color:'black',transform:'rotate(180deg)'}}/>
            </div>
        );
      }

    const NextArrow = props => {
        const { className, onClick } = props;
        return (
            <div className={className} onClick={onClick}>
                <ArrowForwardIos style={{height:'20px',color:'black'}}/>
            </div>
        );
      }

      const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        nextArrow: <PrevArrow/>,
        prevArrow: <NextArrow/>
      }
  


  const data = useStaticQuery(graphql`
  query{
            allCitiesJson {
              nodes{
                background
                city
                country
                id
                objects
              }
            }
          }
  `)

  return(
      
     <div className="Cities">
        
          <div className="Cities-list">
            <Slider {...settings}>

                {data.allCitiesJson.nodes.map((el,index) => (
                  <div>
                    <div id={el.id} index={index} className="city">
                      <div className="city-img" style={{ 
                        backgroundImage: `url("${el.background}")` 
                      }} >
                        <div className="city-img__data">
                          <div className="city-name">{el.city}</div>
                          <div className="city-country">{el.country}</div>
                        </div>
                      </div>
                      <div className="city-obj__wrapper">
                        <p className="city-obj__list">
                        {el.objects.map((obj,indexO)=> (
                          <Link index={indexO}  style={{ color: '#0071c2' }} className="link" to="/">
                          <p className="obj">{obj}</p></Link>
                        ))}</p>
                      </div>
                    </div>
                    
                  </div>
                ))}
            </Slider>
          </div>
        </div>
  )
}
export default CitiesH 




