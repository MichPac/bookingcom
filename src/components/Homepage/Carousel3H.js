import React from "react"
import { useStaticQuery, graphql } from "gatsby";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Image from 'gatsby-image';
import { ArrowForwardIos } from '@material-ui/icons';



const Carousel3H = () => {

  const PrevArrow = props => {
    const { className, onClick } = props;
    return (
        <div className={className} onClick={onClick}>
            <ArrowForwardIos style={{height:'20px', color:'black',transform:'rotate(180deg)'}}/>
        </div>
    );
  }

  const NextArrow = props => {
    const { className, onClick } = props;
    return (
        <div className={className} onClick={onClick}>
            <ArrowForwardIos style={{height:'20px',color:'black'}}/>
        </div>
    );
  }


  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    prevArrow: <PrevArrow/>,
    nextArrow: <NextArrow/>
  }
  


  const data = useStaticQuery(graphql`
  query{
    allHousesPeopleLoveJson {
        nodes {
          id
          name
          opinions
          place
          score
          startPrice
          image{
            childImageSharp{
            fixed(width:310, height:300){
              ...GatsbyImageSharpFixed
              }
            }  
          }
        }
      }
    }
  `)

  return(
      
    <div className="Carousel3">
        <div className="Carousel3-title">
                <h2>Domy które goście kochają</h2>
            </div>
          <div className="Carousel3-list">
            <Slider {...settings}>

                {data.allHousesPeopleLoveJson.nodes.map(el => (
                  <React.Fragment>
                    <div className="house">
                      <Image fixed={el.image.childImageSharp.fixed} alt={el.name}/>
                      <div className="house-info">
                        <div className="house-name">{el.name}</div>
                        <div className="house-location">{el.place}</div>
                        <div className="house-prices">Ceny od {el.startPrice} zł</div>
                        <div className="house-Op">
                          <span className="house-score">{el.score}</span>
                          <span className="house-desc">Znakomity</span>
                          <span className="house-opinions">{el.opinions} opinii</span>
                        </div>
                      </div>
                    </div>
                    
                  </React.Fragment>
                ))}


                <div className="house">
                      <div>Mamy o wiele więcej domów i apartamentów, które Ci się spodobają</div>
                    </div>
            </Slider>
          </div>
        </div>
  )
}
export default Carousel3H



