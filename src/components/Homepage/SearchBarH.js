import React, { useEffect, useState } from 'react';
import HPopUp from '../../elements/HsearchFormEl/popUp'
import 'rsuite/dist/styles/rsuite-default.css';
import HCalendar from '../../elements/HsearchFormEl/Calendar';


const SearchBarH = ({setSearchResult,searchData,setSearchData,handleChange,changeVal}) => {

    const[bannerV,setBannerV]=useState(false)


    const handleSubmit = (val) => {
        if(searchData.place.trim()===''){
            setBannerV(true)
        }
        else{
            setSearchResult(false)
        }
    }

    return (
        <div className="searchBarH">
            <div className="searchBarH-wrapper">
                <div className="searchBarH-title">
                    <p className="title">Znajdź oferty hoteli, domów i wielu innych obiektów... </p>
                    <p className="subtitle">Od przytulnych domków wiejskich po modne apartamenty w mieście </p>
                </div>
                <div className="searchBarH-form">
                    <div>
                        <div className="searchBarH-bar">
                                <label className="where-to">
                                    <input onChange={(e)=>{handleChange(e);setBannerV(true)}} 
                                    value={searchData.place} 
                                    placeholder="Dokąd się wybierasz?"
                                    />
                                    {searchData.place.length === 0 && bannerV===true ? 
                                    <div className='banner'>
                                        <span className='banner-el'></span>
                                        Wybierz cel podrózy aby rozpocząć wyszukiwanie.
                                    </div> : null
                                    }
                                    {searchData.place ?
                                        <button onClick={()=>{setSearchData({...searchData,place:''})}} data-clear="" className='del-btn'>
                                            <svg aria-hidden="true" class="bk-icon -streamline-close sb-destination__clear-icon" fill="#333333" focusable="false" height="20" role="presentation" width="20" viewBox="0 0 24 24"><path d="M13.31 12l6.89-6.89a.93.93 0 1 0-1.31-1.31L12 10.69 5.11 3.8A.93.93 0 0 0 3.8 5.11L10.69 12 3.8 18.89a.93.93 0 0 0 1.31 1.31L12 13.31l6.89 6.89a.93.93 0 1 0 1.31-1.31z"></path></svg>
                                        </button> : null 
                                    }
                                </label>
                                <HCalendar 
                                    searchData={searchData}
                                    setSearchData={setSearchData}
                                />
                                <HPopUp
                                    changeVal={changeVal}
                                    searchData={searchData}

                                />
                                <button onClick={()=>handleSubmit(bannerV)} className="search-btn">
                                    <div>Szukaj</div>
                                </button> 
                        </div>
                        <div className="checkbox">
                                <label class="container">
                                    <input type="checkbox"/>
                                    <span class="checkmark"></span>
                                    <span className="text">Podróżuję służbowo</span>
                                </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SearchBarH




