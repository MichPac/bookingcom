import React from 'react'

function ProposalOffers() {
    return (
        <div className="mainContainerPO">
            <div className="upperContentPO">

                <div className="upperContentPO-pic1">
                    <div className="pic1"></div>
                    <div className="PO-desc">
                        <div className="Po-desc__city">
                            <div className="Po-desc__cityUp">
                                <p>Gdynia</p>
                                <img src="https://cf.bstatic.com/static/img/flags/24/pl/9f4e1f743f84357d0fbca357b2be2560ef50da34.png" alt="Polska" />
                            </div>
                            <span>1 311 obiektów</span>
                        </div>
                        <div className="PO-avg__price">
                            <span>Średnia cena</span>
                            <span className="PO-avg__priceP">265 zł</span>
                        </div>
                    </div>
                </div>
                <div className="upperContentPO-pic1">
                <div className="pic2"></div>
                    <div className="PO-desc">
                        <div className="Po-desc__city">
                            <div className="Po-desc__cityUp">
                                <p>Sopot</p>
                                <img src="https://cf.bstatic.com/static/img/flags/24/pl/9f4e1f743f84357d0fbca357b2be2560ef50da34.png" alt="Polska" />
                            </div>
                            <span>2 001 obiektów</span>
                        </div>
                        <div className="PO-avg__price">
                            <span>Średnia cena</span>
                            <span className="PO-avg__priceP">344 zł</span>
                        </div>
                    </div>
                </div>
                
            </div>
            <div className="upperContentPO">
            <div className="upperContentPO-pic1">
            <div className="pic3"></div>
            <div className="PO-desc">
                <div className="Po-desc__city">
                    <div className="Po-desc__cityUp">
                        <p>Stronie Śląskie</p>
                        <img src="https://cf.bstatic.com/static/img/flags/24/pl/9f4e1f743f84357d0fbca357b2be2560ef50da34.png" alt="Polska" />
                    </div>
                    <span>149 obiektów</span>
                </div>
                <div className="PO-avg__price">
                    <span>Średnia cena</span>
                    <span className="PO-avg__priceP">230 zł</span>
                </div>
            </div>
        </div>

        <div className="upperContentPO-pic1">
        <div className="pic4"></div>
            <div className="PO-desc">
                <div className="Po-desc__city">
                    <div className="Po-desc__cityUp">
                        <p>Wisła</p>
                        <img src="https://cf.bstatic.com/static/img/flags/24/pl/9f4e1f743f84357d0fbca357b2be2560ef50da34.png" alt="Polska" />
                    </div>
                    <span>330 obiektów</span>
                </div>
                <div className="PO-avg__price">
                    <span>Średnia cena</span>
                    <span className="PO-avg__priceP">245zł</span>
                </div>
            </div>
        </div>
        <div className="upperContentPO-pic1">
        <div className="pic5"></div>
            <div className="PO-desc">
                <div className="Po-desc__city">
                    <div className="Po-desc__cityUp">
                        <p>Poznań</p>
                        <img src="https://cf.bstatic.com/static/img/flags/24/pl/9f4e1f743f84357d0fbca357b2be2560ef50da34.png" alt="Polska" />
                    </div>
                    <span>1 045 obiektów</span>
                </div>
                <div className="PO-avg__price">
                    <span>Średnia cena</span>
                    <span className="PO-avg__priceP">236zł</span>
                </div>
            </div>
        </div>
            </div>
        </div>
    )
}

export default ProposalOffers

