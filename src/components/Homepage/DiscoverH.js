import React,{useState} from 'react'
import { useStaticQuery, graphql ,Link} from "gatsby";


const DiscoverH = () => {


  const[listNum,setListNum] = useState({f:0,s:5})
  const[active,setActive] = useState(1)
  const Arr = [0,1,2,3,4,5,6,7,8,9,10]

  const handleClick = (f,s) => {
    setListNum({f:f,s:s})
    setActive((s/5))
  }


  const data = useStaticQuery(graphql`
  query{
            allCountriesJson {
                nodes {
                  id
                  imgPath
                  name
                  desc
                }
              }
          }
  `)

  return(
      
     <div className="DiscoverH">
            <div className="DiscoverH-title">
                <h3>Odkrywaj</h3>
            </div>
            <div className="DiscoverH-bar">
                <div className="DiscoverH-bar__list">
                      {
                        Arr.map(el => (
                          <div onClick={()=>handleClick((el*5),(el*5+5))}  className={active === (el+1) ? "list-el active" : "list-el"}>{el+1}</div>
                        ))
                      }
                    <div className="list-more">Więcej krajów</div>
                </div>
            </div>
            <div className="DiscoverH-list">
            {data.allCountriesJson.nodes.slice(listNum.f,listNum.s).map(el => (
                <Link className="Link" to="">
                    <div className="DiscoverH-list__el">
                        <img src={el.imgPath} alt={el.name}></img>
                        <div className="city-name">{el.name}</div>
                    </div>
                </Link>
            ))}
            </div>
        </div>
  )
}
export default DiscoverH
