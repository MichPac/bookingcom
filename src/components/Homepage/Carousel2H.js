import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { ArrowForwardIos } from '@material-ui/icons';
import { useStaticQuery, graphql } from "gatsby";
import Slider from "react-slick";



const Carousel2H = () => {

    const PrevArrow = props => {
        const { className, onClick} = props;
        return (
            <div className={className} onClick={onClick}>
                <ArrowForwardIos style={{height:'20px', color:'black',transform:'rotate(180deg)'}}/>
            </div>
        );
      }

    const NextArrow = props => {
        const { className, onClick } = props;
        return (
            <div className={className} onClick={onClick}>
                <ArrowForwardIos style={{height:'20px',color:'black'}}/>
            </div>
        );
      }

    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        prevArrow: <PrevArrow/>,
        nextArrow: <NextArrow/>
      }
  


  const data = useStaticQuery(graphql`
  query{
    allCarousel2Json {
        nodes {
          hotels
          id
          imgPath
          place
        }
      }
    }
  `)

  return(
      
    <div className="Carousel1">
            <div className="Carousel-title">
                <h3>Polska - odkryj to miejsce</h3>
                <p>Te popularne miejsca mają wiele do zaoferowania</p>
            </div>
            <div className="Carousel-list">
                <Slider {...settings}> 
                    {data.allCarousel2Json.nodes.map((el,index) => (
                        <div className="Cath">
                            <div id={el.id} index={index} className="Cath-elements">   
                                <img src={el.imgPath}/>
                                <h4>{el.place}</h4>
                                <p>{el.hotels}</p>
                            </div>
                        </div>
                    ))}
                </Slider>
            </div>
        </div>
  )
}
export default Carousel2H


