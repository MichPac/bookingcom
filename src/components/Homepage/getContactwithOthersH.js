import React from "react";
import { useStaticQuery, graphql } from "gatsby";


const GetContactWithOthersH = () => {
  const data = useStaticQuery(graphql`
  query{
    allGetContactwOthersJson {
      nodes {
        id
        title
        travelersNum
        subtitle
        imgPath
      }
    }
  }
  `)

  return(
      
    <div className="GetContactWithOthers">
        <div className="GetContactWithOthers-title">
                <h2>Nawiąz kontakt z innymi podrózującymi</h2>
            </div>
          <div className="GCWO-list">
                {data.allGetContactwOthersJson.nodes.map(el => (
                  <div className="GCWO-list__el">
                    <div index={el.id} className="el">
                      <img src={el.imgPath} alt={el.title}/>
                      <div className="el-info">
                        <div className="el-title">{el.title}</div>
                        <div className="el-subtitle">{el.subtitle}</div>
                        <div className="el-travelersNum">
                        {el.travelersNum} podrózujących
                        </div>
                      </div>
                    </div>
                    
                  </div>
                ))}
          </div>
        </div>
  )
}
export default GetContactWithOthersH

