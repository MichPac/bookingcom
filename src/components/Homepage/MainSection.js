import React from 'react';
import ProposalOffers from './ProposalOffers';
import Carousel1 from './Carousel1';
import Carousel2H from './Carousel2H';
import InspirationComp from './InspirationComp';
import Carousel3H from './Carousel3H';
import GetContactWithOthersH from './getContactwithOthersH';
import CitiesH from './CitiesH';
import FavPlacesH  from './FavPlacesH';
import DiscoverH from './DiscoverH';
import AdviceH from './adviceH';

const MainSection = () => {
    return (
        <main className="main-section">
            <AdviceH/>
            <ProposalOffers/>
            <Carousel1/>
            <Carousel2H/>
            <InspirationComp/>
            <Carousel3H/>
            <GetContactWithOthersH/>
            <CitiesH/>
            <FavPlacesH/>
            <DiscoverH/>
        </main>
    )
}

export default MainSection
