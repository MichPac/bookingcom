import React from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { ArrowForwardIos } from '@material-ui/icons';
import { useStaticQuery, graphql } from "gatsby";



const Carousel1 = () => {

    const PrevArrow = props => {
        const { className, onClick } = props;
        return (
            <div className={className} onClick={onClick}>
                <ArrowForwardIos style={{height:'20px', color:'black',transform:'rotate(180deg)'}}/>
            </div>
        );
      }

    const NextArrow = props => {
        const { className, onClick } = props;
        return (
            <div className={className} onClick={onClick}>
                <ArrowForwardIos style={{height:'20px',color:'black'}}/>
            </div>
        );
      }

    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        prevArrow: <PrevArrow/>,
        nextArrow: <NextArrow/>
      }
  


  const data = useStaticQuery(graphql`
  query{
    allCarousel1Json {
        nodes {
          name
          imgPath
          objects
          id
        }
      }
    }
  `)

  return(
      
    <div className="Carousel1">
            <div className="Carousel-title">
                <h2>Szukaj według rodzaju obiektu</h2>
            </div>
            <div className="Carousel-list">
                <Slider {...settings}> 
                    {
                        data.allCarousel1Json.nodes.map((el,index)=> (
                            <div index={index} className="Cath">
                                <div className="Cath-elements">   
                                    <img src={el.imgPath} alt={el.name}/>
                                    <h4>{el.name}</h4>
                                    <p>{el.objects}</p>
                                </div>
                            </div>
                        ))
                    }
                </Slider>
            </div>
        </div>
  )
}
export default Carousel1
