import React from 'react'
import { StaticQuery, graphql,Link } from "gatsby"


export default function UpContentCR() {

  
  return (
    <StaticQuery
      query={graphql`
        query{
            allCarRentalJson {
                nodes {
                  carClass
                  city
                  city2
                  id
                  imgPath
                  rentalNum
                  startPrice
                }
              }
          }
      `}
      render={data => (
        
        <div className="UpContentCR">
        <div className="UpContentCR-title">
            <h2>Popularne miejsca do wypożyczenia samochodu</h2>
        </div>
        <div className="UpContentCR-list">
            {data.allCarRentalJson.nodes.slice(0,6).map(el => (
                <div className="UpContentCR-el">
                <div className="UpContentCR-el__img">
                    <img src={el.imgPath}/>
                </div>
                <div className="UpContentCR-el__info">
                    <div className="city">
                        <h3>{el.city}</h3></div>
                    <div className="desc">
                        <h4>Wypożyczenie samochodu możliwe w {el.rentalNum} miejscach odbioru</h4>
                    </div>
                    <div className="startPrice">
                        <div className="startPrice-icon">
                        <svg class="bk-icon -iconset-car_front" fill="#6B6B6B" height="40" width="40" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M109.2 48.9L100 26a16 16 0 0 0-14.8-10H42.8A16 16 0 0 0 28 26l-9.1 23A16 16 0 0 0 8 64v24a8 8 0 0 0 8 8v8a8 8 0 0 0 16 0v-8h64v8a8 8 0 0 0 16 0v-8a8 8 0 0 0 8-8V64a16 16 0 0 0-10.8-15.1zM35.4 29a8 8 0 0 1 7.4-5h42.4a8 8 0 0 1 7.4 5l7.6 19H27.8zM26 76a10 10 0 1 1 10-10 10 10 0 0 1-10 10zm76 0a10 10 0 1 1 10-10 10 10 0 0 1-10 10z"></path></svg>
                        </div>
                        <div className="startPrice-desc">
                            <div><span>Od {el.startPrice} zł za dzień</span></div>
                            <div><span>Samochód klasy {el.carClass}</span></div>
                        </div>
                    </div>
                    <div className="Link">
                        <Link to="/" className="link">Wyszukaj wypożyczalnie samochodów w {el.city2} </Link>
                    </div>
                </div>
            </div>
            ))}
        </div>
    </div>
      )}
    />
  )
}

