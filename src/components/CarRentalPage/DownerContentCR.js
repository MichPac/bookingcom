import React from 'react'
import { StaticQuery, graphql,Link } from "gatsby"


export default function DwonContentCR() {

  
  return (
    <StaticQuery
      query={graphql`
        query{
            allCarRentalJson {
                nodes {
                  id
                    imgPath
                  city
                  startPrice
                }
              }
          }
      `}
      render={data => (
        
        <div className="DownerContentCR">
            <div className="DownerContentCR-title">
                <h3>Najlepsze miejsca na świecie na wynajem samochodu</h3>
            </div>
            <div className="DownerContentCR-list">
            {data.allCarRentalJson.nodes.slice(6,19).map(el => (
                <div index={el.id} className="DownerContentCR-el">
                        <img src={el.imgPath} alt={el.city}></img>
                        <Link className="info">
                            <div className="info-desc">
                                <h3>{el.city}</h3>
                                <h4>Wynajem samochodu od {el.startPrice} zł za dzień</h4>
                            </div>
                        </Link>
                </div>
              ))}
                
            </div>
            
        </div>
      )}
    />
  )
}
