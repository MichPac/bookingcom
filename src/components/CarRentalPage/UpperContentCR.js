import React from 'react'

function UpperContentCR() {
    return (
        <div className="PopularRentals">
            <div className="PopularRentals-title">
                <h2>Popularne wypożyczalnie</h2>
            </div>
            <div className="PopularRentals-list">
                    <div className="PopularRentals-list__el">
                        <img src="https://cdn.rcstatic.com/images/supplier_logos/europcar_logo_lrg.gif"></img>
                    </div>
                    <div className="PopularRentals-list__el">
                        <img src="https://cdn.rcstatic.com/images/supplier_logos/alamo_logo_lrg.gif"></img>
                    </div>
                    <div className="PopularRentals-list__el">
                        <img src="https://cdn.rcstatic.com/images/suppliers/flat/sixt_logo_lrg.gif"></img>
                    </div>
                    <div className="PopularRentals-list__el">
                        <img src="https://cdn.rcstatic.com/images/supplier_logos/avis_logo_lrg.gif"></img>
                    </div>
                    <div className="PopularRentals-list__el">
                        <img src="https://cdn.rcstatic.com/images/supplier_logos/enterprise_logo_lrg.gif"></img>
                    </div>
                    <div className="PopularRentals-list__el">
                        <img src="https://cdn.rcstatic.com/images/supplier_logos/dollar_logo_lrg.gif"></img>
                    </div>
                    <div className="PopularRentals-list__el">
                        <img src="https://cdn.rcstatic.com/images/supplier_logos/thrifty_logo_lrg.gif"></img>
                    </div>
                    <div className="PopularRentals-list__el">
                        <img src="https://cdn.rcstatic.com/images/suppliers/flat/sicily_by_car_logo_lrg.gif"></img>
                    </div>
            </div>
        </div>
    )
}

export default UpperContentCR
