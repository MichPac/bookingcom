import React from 'react'
import { StaticQuery, graphql } from "gatsby"
import AccordionCR from '../../elements/accordion/AccordionCR'

export default function DownContentCR() {

  
    return (
      <StaticQuery
        query={graphql`
          query{
            allAccordionCrJson {
                nodes {
                  question
                  p1
                  p2
                  p3
                  p4
                  l1
                  l2
                  l3
                  l4
                  id
                }
              }
            }
        `}
        render={data => (
          <div className="DownContentCR">
                                <div className="DownContentCR-container">
                                    <div className="findMore">
                                            <div className="findMore-title">
                                                <h2>Często zadawane pytania</h2>
                                            </div>
                                    </div>
                                    <div className="questions">
                                    {data.allAccordionCrJson.nodes.map(el => (
                                        <AccordionCR {...el}/>
                                    ))}
                                    </div>
                                </div>
                </div>
        )}
      />
    )
  }



//   <div className="question-wrapper border-bottom">
//   <div className="question-title">Czego potrzebuję, aby wypożyczyć samochód?
//       <span>
//           <svg fill="#6b6b6b" class="bk-icon -iconset-navarrow_down bui-accordion__icon" height="30" role="presentation" width="30" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M64 88L33.2 57.2a4 4 0 0 1 5.6-5.7L64 76.7l25.2-25.2a4 4 0 0 1 5.6 5.7z"></path></svg>
//       </span>
//   </div>
//   <div style={{display:"none"}}  className="question-answer">
//   <p></p><p>Podczas dokonywania rezerwacji auta potrzebujesz jedynie swojej karty kredytowej lub debetowej.</p> <p>Do odbioru auta będziesz potrzebować:</p> <ul> <li>Paszportu</li> <li>Kuponu rezerwacji</li> <li>Ważnego prawa jazdy każdego z kierowców</li> <li>Karty kredytowej na imię i nazwisko głównego kierowcy (niektóre wypożyczalnie akceptują karty debetowe, jednakże większość z nich tego nie robi).</li> </ul> <p>Ważne: upewnij się, że zapoznałeś się z warunkami wynajmu samochodu, ponieważ każda wypożyczalnia posiada inne warunki. Na przykład, niektóre wypożyczalnie wymagają dodatkowego dowodu potwierdzającego tożsamość, akceptują niektóre rodzaje kart kredytowych lub nie wypożyczają samochodów osobom, które posiadają prawo jazdy krócej niż 36 miesięcy.</p><p></p>
//   </div>
// </div>
// <div className="question-wrapper border-bottom">
//   <div className="question-title">Czy jestem w wieku, który upoważnia do wynajęcia samochodu?
//       <span>
//           <svg  fill="#6b6b6b" class="bk-icon -iconset-navarrow_down bui-accordion__icon" height="30" role="presentation" width="30" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M64 88L33.2 57.2a4 4 0 0 1 5.6-5.7L64 76.7l25.2-25.2a4 4 0 0 1 5.6 5.7z"></path></svg>
//       </span>
//   </div>
//   <div  className="question-answer">
//       <p>Większość wypożyczalni wymaga, aby kierowca miał min. 21 lat (niektóre wypożyczalnie wynajmują samochody młodszym kierowcom). Jednakże jeśli masz mniej niż 25 lat najprawdopodobniej zostaniesz poproszony o uiszczenie dodatkowej opłaty.</p>
//   </div>
// </div>
// <div className="question-wrapper border-bottom">
//   <div className="question-title">Czy mogę dokonać rezerwacji dla mojego partnera, przyjaciela, znajomego itp.?
//       <span>
//           <svg fill="#6b6b6b" class="bk-icon -iconset-navarrow_down bui-accordion__icon"  height="30" role="presentation" width="30" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M64 88L33.2 57.2a4 4 0 0 1 5.6-5.7L64 76.7l25.2-25.2a4 4 0 0 1 5.6 5.7z"></path></svg>
//       </span>
//   </div>
//   <div  className="question-answer">
//   <p>Oczywiście. Wystarczy wypełnić formularz „Dane kierowcy” podczas dokonywania rezerwacji.</p>
// </div>
// </div>
// <div className="question-wrapper  border-bottom">
//   <div className="question-title">
//   Jak wybrać odpowiedni samochód?
//       <span>
//           <svg fill="#6b6b6b" class="bk-icon -iconset-navarrow_down bui-accordion__icon" height="30" role="presentation" width="30" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M64 88L33.2 57.2a4 4 0 0 1 5.6-5.7L64 76.7l25.2-25.2a4 4 0 0 1 5.6 5.7z"></path></svg>
//       </span>
//   </div>
//   <div  className="question-answer">
//   <p></p><ul> <li>Pomyśl o tym, dokąd się wybierasz. SUV może być doskonałym wyborem na przejażdżkę autostradami w Teksasie, za to mniejszy samochód będzie najprawdopodobniej wygodniejszy do jazdy po Rzymie.</li> <li>Zobacz, co sądzą inni. Na naszej stronie znajdziesz mnóstwo opinii innych podróżujących, z których dowiesz się, co podobało i nie podobało się innym klientom w danej wypożyczalni.</li> <li>Pamiętaj o skrzyni biegów. W niektórych krajach prawie każdy samochód wyposażony jest w manualną skrzynię biegów, za to w innych normę stanowią automatyczne skrzynie biegów. Upewnij się, że rezerwujesz samochód, który będziesz potrafił poprowadzić.</li> </ul><p></p>
//   </div>
// </div>
// <div className="question-wrapper">
//   <div className="question-title">
//   Czy cena wynajmu obejmuje wszystkie opłaty?
//       <span>
//           <svg  fill="#6b6b6b" class="bk-icon -iconset-navarrow_down bui-accordion__icon" height="30" role="presentation" width="30" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M64 88L33.2 57.2a4 4 0 0 1 5.6-5.7L64 76.7l25.2-25.2a4 4 0 0 1 5.6 5.7z"></path></svg>
//       </span>
//   </div>
//   <div style={{display:"none"}} className="question-answer">
//   <p></p><p>Cena widoczna na stronie obejmuje samochód, obowiązkowe ubezpieczenie (np. ubezpieczenie od kradzieży oraz ubezpieczenie AC) oraz możliwe opłaty, które są zazwyczaj uiszczane przy odbiorze (np. opłata za zwrot samochodu w innej lokalizacji, opłaty lotniskowe oraz podatki lokalne).</p> <p>Obejmuje ona również opłaty za wybrane przez Ciebie dodatki (np. nawigację GPS czy fotelik dziecięcy).</p> <p>Cena wynajmu nie obejmuje natomiast dodatkowego ubezpieczenia, które możesz kupić podczas odbioru samochodu.</p> <p>Wskazówka: składowe ceny dostępne są na stronie płatności.</p><p></p>
//   </div>
// </div>

