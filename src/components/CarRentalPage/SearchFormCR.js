import React, { useState } from 'react'
import CRCalendar from '../../elements/CRsearchFormEl/Calendar'

const SearchFormCR = () => {

    const[radio,setRadio]= useState('sameP')

    const today = new Date()
    const tomorrow = new Date(today)
    const days3later = new Date(today)
    tomorrow.setDate(tomorrow.getDate() + 1)
    days3later.setDate(tomorrow.getDate() + 3)

    const[inputData,setInputData] = useState({
        pickUpPlace:'',
        dropUpPlace:'',
        date1: tomorrow,
        date2: days3later,
        h1:'10',
        m1:'00',
        h2:'10',
        m2:'00'
    })




    const handleChangeVal = (e) => {
        const name = e.target.name
        setInputData({...inputData,[name]:e.target.value})
    }


    return(
        <div className="SearchFormCR">
            <div className="SearchFormCR-wraper">
                <div className="SearchFormCR-title">
                    <div className="title">Wynajem samochodu na każdy rodzaj podróży</div>
                    <div className="sub-title">Porównaj oferty największych wypożyczalni aut</div>
                </div>
                <div className="SearchFormCR-form">
                    <form>
                        <div className="radio-btns">
                            <label class="containerR"> 
                                <input onClick={()=>setRadio('sameP')} type="radio" checked={radio==='sameP' ? true: false} name="radio"/>
                                <span class="checkmarkR"></span>
                                Zwrot w tym samym miejscu
                            </label>
                            <label class="containerR2">
                                <input onClick={()=>setRadio('diffP')} type="radio" checked={radio==='diffP' ? true: false} name="radio"/>
                                <span class="checkmarkR2"></span>
                                Zwrot w innym miejscu
                            </label>
                        </div>
                        <div className="search-bar">
                            <div className="place-container">
                                <label className={radio=='sameP'?'place-SP':'place'}>
                                    <span>  
                                        <svg class="bk-icon -iconset-car_front" fill="#ccc" height="28" width="28" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M109.2 48.9L100 26a16 16 0 0 0-14.8-10H42.8A16 16 0 0 0 28 26l-9.1 23A16 16 0 0 0 8 64v24a8 8 0 0 0 8 8v8a8 8 0 0 0 16 0v-8h64v8a8 8 0 0 0 16 0v-8a8 8 0 0 0 8-8V64a16 16 0 0 0-10.8-15.1zM35.4 29a8 8 0 0 1 7.4-5h42.4a8 8 0 0 1 7.4 5l7.6 19H27.8zM26 76a10 10 0 1 1 10-10 10 10 0 0 1-10 10zm76 0a10 10 0 1 1 10-10 10 10 0 0 1-10 10z"></path></svg>
                                    </span>
                                    <input name="pickUpPlace" onChange={(e)=>handleChangeVal(e)} placeholder="Miejsce odbioru"/>
                                </label>
                                {radio==='diffP' &&
                                    <label className="place1">
                                        <span>  
                                            <svg class="bk-icon -iconset-car_front" fill="#ccc" height="28" width="28" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M109.2 48.9L100 26a16 16 0 0 0-14.8-10H42.8A16 16 0 0 0 28 26l-9.1 23A16 16 0 0 0 8 64v24a8 8 0 0 0 8 8v8a8 8 0 0 0 16 0v-8h64v8a8 8 0 0 0 16 0v-8a8 8 0 0 0 8-8V64a16 16 0 0 0-10.8-15.1zM35.4 29a8 8 0 0 1 7.4-5h42.4a8 8 0 0 1 7.4 5l7.6 19H27.8zM26 76a10 10 0 1 1 10-10 10 10 0 0 1-10 10zm76 0a10 10 0 1 1 10-10 10 10 0 0 1-10 10z"></path></svg>
                                        </span>
                                        <input name="dropUpPlace" onChange={(e)=>handleChangeVal(e)} placeholder="Miejsce odbioru"/>
                                    </label>
                                }
                            </div>
                            <CRCalendar 
                                inputData={inputData}
                                setInputData={setInputData}
                                handleChangeVal={handleChangeVal}
                            />
                            <div className="button">
                                <div>Szukaj</div>
                            </div>
                        </div>
                        <div className="checkbox">
                            <label class="container">
                                <input checked="true" type="checkbox"/>
                                <span class="checkmark"></span>
                                Kierowca w wieku 30-65 lat
                                <svg class="bk-icon -iconset-info_sign" height="28" width="28" viewBox="0 0 128 128" role="presentation" aria-hidden="true" focusable="false"><path d="M49.4 85l4.2-17.2c.7-2.7.8-3.8 0-3.8a29 29 0 0 0-8.8 3.8l-1.8-3A48 48 0 0 1 66.7 53c3.7 0 4.3 4.3 2.5 11l-5 18c-.7 3.3-.3 4.3.5 4.3a19 19 0 0 0 8.2-4L75 85c-8.6 8.7-18.2 12-21.8 12s-6.4-2.3-3.8-12zM75 36a9.2 9.2 0 0 1-9.2 9c-4.4 0-7-2.7-6.8-7a9 9 0 0 1 9.1-9c4.6 0 6.9 3.2 6.9 7z"></path><path d="M62 16a48 48 0 1 1-48 48 48 48 0 0 1 48-48m0-8a56 56 0 1 0 56 56A56 56 0 0 0 62 8z"></path></svg>
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default SearchFormCR



