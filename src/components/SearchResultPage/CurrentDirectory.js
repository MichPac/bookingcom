import React, {useState,useEffect} from 'react'
import {Link,graphql,useStaticQuery} from 'gatsby'


const CurrentDirectory = ({place,showLandingPage}) => {

  const data = useStaticQuery(graphql`
    query{
        allObjectsSJson {
            nodes {
            country
            region
            place
            }
        }
    }
    `)

    const placesList =  data.allObjectsSJson.nodes;
    console.log(placesList)

    const [placeData,setPlaceData] = useState({
        country:'',
        region:'',
        city:''
    })
    
    useEffect(()=>{
        displayNav()
    },[place])

    const displayNav = () => {
        for(let i=0;i<placesList.length;i++){
            if(place===placesList[i].country){
                console.log(place)
                setPlaceData({
                    country: place,
                    region:'',
                    city:''
                })
            }
            else if(place===placesList[i].region){
                console.log(place)
                setPlaceData({
                    country: placesList[i].country,
                    region: place,
                    city:''
                })
            }
            else if(place===placesList[i].place){
                setPlaceData({
                    country: placesList[i].country,
                    region: placesList[i].region,
                    city: place
                })
            }
        }
    }

    const handleDisplayVal = (val) => {
        var splitStr = val.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        return splitStr.join(' ')
    }

  return(
      
    <div  className="current-directory">
                    <div className="current-directory__el">
                        <div onClick={showLandingPage}>
                            <Link className="link2" to="/">Strona główna</Link>
                        </div>
                        <div className="arrow">
                            <svg class="bk-icon -iconset-navarrow_right bui-breadcrumb__icon" height="16" role="presentation" width="16" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M54.3 96a4 4 0 0 1-2.8-6.8L76.7 64 51.5 38.8a4 4 0 0 1 5.7-5.6L88 64 57.2 94.8a4 4 0 0 1-2.9 1.2z"></path></svg>
                        </div>
                    </div>
                        {placeData.country !== ' ' ? 
                            <div className="current-directory__el">   
                                <div>
                                    <Link className="link2" to="/countryname">{handleDisplayVal(placeData.country)}</Link>
                                </div>
                                <div className="arrow">
                                    <svg class="bk-icon -iconset-navarrow_right bui-breadcrumb__icon" height="16" role="presentation" width="16" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M54.3 96a4 4 0 0 1-2.8-6.8L76.7 64 51.5 38.8a4 4 0 0 1 5.7-5.6L88 64 57.2 94.8a4 4 0 0 1-2.9 1.2z"></path></svg>
                                </div>
                            </div> : null
                        }
                        
                        {placeData.region !== '' ? 
                            <div className="current-directory__el">
                            <div className="arrow">
                                <Link className="link2" to="/regionname">{placeData.region}</Link>
                            </div>
                            <div className="arrow">
                            <svg class="bk-icon -iconset-navarrow_right bui-breadcrumb__icon" height="16" role="presentation" width="16" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M54.3 96a4 4 0 0 1-2.8-6.8L76.7 64 51.5 38.8a4 4 0 0 1 5.7-5.6L88 64 57.2 94.8a4 4 0 0 1-2.9 1.2z"></path></svg>
                            </div>
                        </div>  : null}

                         {placeData.city !== '' ? 
                            <div className="current-directory__el">
                            <div>
                                <Link className="link2" to="/cityname">{handleDisplayVal(placeData.city)}</Link>
                            </div>
                            <div className="arrow">
                            <svg class="bk-icon -iconset-navarrow_right bui-breadcrumb__icon" height="16" role="presentation" width="16" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M54.3 96a4 4 0 0 1-2.8-6.8L76.7 64 51.5 38.8a4 4 0 0 1 5.7-5.6L88 64 57.2 94.8a4 4 0 0 1-2.9 1.2z"></path></svg>
                            </div>
                        </div> : null}

                    <div className="current-directory__el">
                        <div className="res">
                            Wyniki wyszukiwania
                        </div>
                    </div>
            </div>
  )
}
export default CurrentDirectory


