import React,{useEffect, useState} from 'react'
import SRCalendar from '../../elements/SRsearchFormEl/Calendar'

const SearchForm = (props) => {


    useEffect(()=>{
        setInputData(
            {
                place: props.searchData.place,
                adults: props.searchData.adults,
                kids: props.searchData.kids,
                rooms: props.searchData.rooms,
                date1: props.searchData.date1,
                date2: props.searchData.date2
            }
        )
    },[props.searchData])


    const[inputData, setInputData] = useState(
        {
            place: '',
            adults: '',
            kids: '',
            rooms: '',
            date1: '',
            date2: ''
        }
    )


    const handleChange = (e) => {
        const name = e.target.name
        setInputData({...inputData,[name]: e.target.value})
    }

    const handleSubmit = () => {
        if(inputData.place.trim()!==''){
            props.setSearchData(inputData)
        }
        else{
            alert('miejsce!')
        }
    }

    const handleDisplayVal = (val) => {
        var splitStr = val.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        return splitStr.join(' ')
    }
    const calculateSpentDays = (val1,val2) => {
        if(val1!==''){
            const monthDay1 = val1.getUTCDate()
            const monthDay2 = val2.getUTCDate()
            const month1 = val1.getMonth()
            const month2 = val2.getMonth()
            let daysSpent
            let nights
            if(month1===month2){
                daysSpent = monthDay2-monthDay1
            }
            else{
                const newTime = new Date(val1.getFullYear(), val1.getMonth()+1, 0);
                const lastDayOfMonth = newTime.getUTCDate()
                daysSpent = (lastDayOfMonth-monthDay1)+monthDay2+1
            }
            return "Pobyt na "+daysSpent+" "+nights
            
        }
    }



    return (
        
                    <div className="form">
                        <div>
                            <p className="form-title">Szukaj</p>
                        </div>
                        <div className="form-input">
                            <label htmlFor="goal">Cel podróży / nazwa obiektu:
                                <div className="place">
                                    <svg aria-hidden="true" class="bk-icon -streamline-magnifying_glass sb-destination__icon" fill="#333333" focusable="false" height="25" role="presentation" width="25" viewBox="0 0 24 24"><path d="M17.464 6.56a8.313 8.313 0 1 1-15.302 6.504A8.313 8.313 0 0 1 17.464 6.56zm1.38-.586C16.724.986 10.963-1.339 5.974.781.988 2.9-1.337 8.662.783 13.65c2.12 4.987 7.881 7.312 12.87 5.192 4.987-2.12 7.312-7.881 5.192-12.87zM15.691 16.75l7.029 7.03a.75.75 0 0 0 1.06-1.06l-7.029-7.03a.75.75 0 0 0-1.06 1.06z"></path></svg>
                                    <input name="place" onChange={(e)=>handleChange(e)} id="goal" type="text" placeholder={handleDisplayVal(inputData.place)}></input>
                                </div>
                            </label>
                        </div>
                        <SRCalendar
                            inputData={inputData}
                            setInputData={setInputData}
                        />
                        <div className="stay-time">
                            <p>{calculateSpentDays(inputData.date1,inputData.date2)}</p>
                        </div>
                        <div className="form-input">
                            
                            <div className="adults">
                                <select onChange={(e)=>handleChange(e)} name="adults" value={inputData.adults} placeholder="1 dorosły">
                                    <option value="1">1 dorosły</option>
                                    <option value="2">2 dorosłych</option>
                                    <option value="3">3 dorosłych</option>
                                    <option value="4">4 dorosłych</option>
                                    <option value="5">5 dorosłych</option>
                                </select>
                            </div>
                            <div className="kidsrooms">
                                <div className="select">
                                    <select onChange={(e)=>handleChange(e)} name="kids" value={inputData.kids} placeholder="bez dzieci">
                                        <option value="0">Bez dzieci</option>
                                        <option value="1">1 dziecko</option>
                                        <option value="2">2 dzieci</option>
                                        <option value="3">3 dzieci</option>
                                        <option value="4">4 dzieci</option>
                                        <option value="5">5 dzieci</option>
                                    </select>
                                </div>
                               <div className="select"> 
                                    <select onChange={(e)=>handleChange(e)} name="rooms" value={inputData.rooms} placeholder="1 pokój">
                                        <option value="1">1 pokój</option>
                                        <option value="2">2 pokoje</option>
                                        <option value="3">3 pokoje</option>
                                        <option value="4">4 pokoje</option>
                                        <option value="5">5 pokoje</option>
                                    </select>
                               </div>
                            </div>
                        </div>
                        <div className="Btraveling">
                            <label class="container">
                                <input type="checkbox"/>
                                <span class="checkmark"></span>
                                <span class="text">Podróżuję służbowo</span>
                            </label>
                                <svg aria-hidden="true" class="bk-icon -streamline-question_mark_circle" fill="#333333" focusable="false" height="18" role="presentation" width="18" viewBox="0 0 24 24"><path d="M9.75 9a2.25 2.25 0 1 1 3 2.122 2.25 2.25 0 0 0-1.5 2.122v1.006a.75.75 0 0 0 1.5 0v-1.006c0-.318.2-.602.5-.708A3.75 3.75 0 1 0 8.25 9a.75.75 0 1 0 1.5 0zM12 16.5a1.125 1.125 0 1 0 0 2.25 1.125 1.125 0 0 0 0-2.25.75.75 0 0 0 0 1.5.375.375 0 1 1 0-.75.375.375 0 0 1 0 .75.75.75 0 0 0 0-1.5zM22.5 12c0 5.799-4.701 10.5-10.5 10.5S1.5 17.799 1.5 12 6.201 1.5 12 1.5 22.5 6.201 22.5 12zm1.5 0c0-6.627-5.373-12-12-12S0 5.373 0 12s5.373 12 12 12 12-5.373 12-12z"></path></svg>
                        </div>
                        <div><button onClick={handleSubmit} className="search-btn">Szukaj</button></div>
                    </div>
    )
}

export default SearchForm