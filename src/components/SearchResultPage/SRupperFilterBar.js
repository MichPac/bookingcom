import React, { useEffect, useState ,useRef} from 'react'


const SRuppperFilterBar = ({setFilters,filters,place,updatedHotelList}) => {

    const [placeName, setPlaceName] = useState()


    const handleDisplayVal = (val) => {
        var splitStr = val.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        return splitStr.join(' ')
    }

    useEffect(()=>{
        const placeUpC = handleDisplayVal(place)
        setPlaceName(placeUpC)
    },[updatedHotelList])

    const handleSort = (val) => {
        setFilters({
            ...filters,
            sortby: val,
        })
    }

    return (
        <React.Fragment>
            <div className="Results-title">
                            <div>
                                <h2>{placeName}: znaleziono: {updatedHotelList.length} obiektów</h2>
                            </div>
                            <div className="map-btn">
                                <div>
                                    <button>Pokaz na mapie</button>
                                </div>
                            </div>
                        </div>
                        <div className="Results-categories">
                            <div onClick={()=>handleSort("chsnBU")} className={filters.sortby === "chsnBU" ? "Cath-active" :"Cath"}>
                                Wybrane przez nas
                            </div>
                            <div onClick={()=>handleSort("lowstPri")} className={filters.sortby === "lowstPri" ? "Cath-active" :"Cath"}>
                                Cena (od najniżej)
                            </div>
                             <div onClick={()=>handleSort("BscrPri")} className={filters.sortby === "BscrPri" ? "Cath-active" :"Cath"}>
                                Najlepsza ocena i najnisza cena
                            </div>
                            <div className="Dots"> 
                                ...
                            </div>
                        </div>
        </React.Fragment>
    )


}

export default SRuppperFilterBar
