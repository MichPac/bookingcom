import React from 'react';
import {Link} from 'gatsby';

const SubFooter = () => {


  return(

              <div className="subfoter">
                  <div className="subfooter-list">
                      <div className="subfooter-list__el"><Link className='link' to="/">O Booking.com</Link></div>
                      <div className="subfooter-list__el"><Link className='link' to="/">Warunki</Link></div>
                      <div className="subfooter-list__el"><Link className='link'  to="/">Oświadczenie o ochronie prywatności i plikach cookie</Link></div>
                      <div className="subfooter-list__el"><Link className='link' to="/">Pomoc w zakresie lotów</Link></div>
                  </div>
              </div>
  )
}
export default SubFooter

