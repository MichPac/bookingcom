import React from 'react'
import { Link } from "gatsby"

const HotelLayout = props => {


    const handleDisplayVal = (val) => {
        var splitStr = val.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        return splitStr.join(' ')
    }

    return (
        <div className="hotel">
                            <div className="img">
                                <img src={props.img} alt={props.place+" hotel"}></img>
                            </div>
                            <div className="info">
                                <div className="name">
                                    {props.name+" "} 
                                    <span><svg fill="#febb02" xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 24 24" focusable="false" aria-hidden="true" role="img">
                                    <path d="M23.555,8.729a1.505,1.505,0,0,0-1.406-.98H16.062a.5.5,0,0,1-.472-.334L13.405,1.222a1.5,1.5,0,0,0-2.81,0l-.005.016L8.41,7.415a.5.5,0,0,1-.471.334H1.85A1.5,1.5,0,0,0,.887,10.4l5.184,4.3a.5.5,0,0,1,.155.543L4.048,21.774a1.5,1.5,0,0,0,2.31,1.684l5.346-3.92a.5.5,0,0,1,.591,0l5.344,3.919a1.5,1.5,0,0,0,2.312-1.683l-2.178-6.535a.5.5,0,0,1,.155-.543l5.194-4.306A1.5,1.5,0,0,0,23.555,8.729Z"></path>
                                    </svg></span>
                                    <span><svg fill="#febb02" xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 24 24" focusable="false" aria-hidden="true" role="img">
                                    <path d="M23.555,8.729a1.505,1.505,0,0,0-1.406-.98H16.062a.5.5,0,0,1-.472-.334L13.405,1.222a1.5,1.5,0,0,0-2.81,0l-.005.016L8.41,7.415a.5.5,0,0,1-.471.334H1.85A1.5,1.5,0,0,0,.887,10.4l5.184,4.3a.5.5,0,0,1,.155.543L4.048,21.774a1.5,1.5,0,0,0,2.31,1.684l5.346-3.92a.5.5,0,0,1,.591,0l5.344,3.919a1.5,1.5,0,0,0,2.312-1.683l-2.178-6.535a.5.5,0,0,1,.155-.543l5.194-4.306A1.5,1.5,0,0,0,23.555,8.729Z"></path>
                                    </svg></span>
                                    <span><svg fill="#febb02" xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 24 24" focusable="false" aria-hidden="true" role="img">
                                    <path d="M23.555,8.729a1.505,1.505,0,0,0-1.406-.98H16.062a.5.5,0,0,1-.472-.334L13.405,1.222a1.5,1.5,0,0,0-2.81,0l-.005.016L8.41,7.415a.5.5,0,0,1-.471.334H1.85A1.5,1.5,0,0,0,.887,10.4l5.184,4.3a.5.5,0,0,1,.155.543L4.048,21.774a1.5,1.5,0,0,0,2.31,1.684l5.346-3.92a.5.5,0,0,1,.591,0l5.344,3.919a1.5,1.5,0,0,0,2.312-1.683l-2.178-6.535a.5.5,0,0,1,.155-.543l5.194-4.306A1.5,1.5,0,0,0,23.555,8.729Z"></path>
                                    </svg></span>
                                    <span style={{display: props.partner===null ? 'none' : 'unset' }}>
                                    <svg aria-hidden="true" class="bk-icon -iconset-thumbs_up_square 
                                    pp-icon-valign--tbottom
                                    js-preferred-property-copy-cached" data-bui-component="Tooltip" data-et-mouseenter="
                                    customGoal:TPOaXGZCHQGPGJIMADXRT:1
                                    " data-tooltip-position="bottom" data-tooltip-text="Ten obiekt uczestniczy w naszym Programie dla Partnerów Preferowanych. Oznacza to, że dostarcza on gościom pozytywnych wrażeń dzięki doskonałej obsłudze i rozsądnym cenom. Obiekty w programie mogą płacić Booking.com nieco wyższą prowizję za możliwość uczestnictwa." fill="#FEBB02" height="24" rel="300" width="24" viewBox="0 0 128 128" role="presentation" focusable="false"><path d="M112 8H16a8 8 0 0 0-8 8v96a8 8 0 0 0 8 8h96a8 8 0 0 0 8-8V16a8 8 0 0 0-8-8zM48 96H24V58h24zm56-25a8.7 8.7 0 0 1-2 6 8.9 8.9 0 0 1 1 4 6.9 6.9 0 0 1-5 7c-.5 4-4.8 8-9 8H56V58l10.3-23.3a5.4 5.4 0 0 1 10.1 2.7 10.3 10.3 0 0 1-.6 2.7L72 52h23c4.5 0 9 3.5 9 8a9.2 9.2 0 0 1-2 5.3 7.5 7.5 0 0 1 2 5.7z"></path></svg>
                                    </span>
                                </div>
                                <div className="location">
                                    <Link className="link3" to=''>{handleDisplayVal(props.place)}
                                    <span> · </span>
                                    Pokaz na mapie
                                    </Link>
                                    <span> · </span>
                                    <span className="km">{props.odcentrum} km od centrum</span>
                                </div>
                                <div className="desc">
                                
                                </div>
                            </div>
                            <div className="others">
                                <div className="opinions-rate">
                                    <div className="opinions">
                                        <div>{props.opinion}</div>
                                        <div className="opinions-number">{props.opinions}</div>
                                    </div>
                                    <div className="rate">
                                        {props.score}
                                    </div>
                                </div>
                                <div>
                                    <Link to={props.path}>
                                        <button className="btn">Zobacz dostępność
                                        <svg fill="#fff" class="bk-icon -streamline-arrow_nav_right" height="16" width="16" viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false"><path d="M9.45 6c.2 0 .39.078.53.22l5 5c.208.206.323.487.32.78a1.1 1.1 0 0 1-.32.78l-5 5a.75.75 0 0 1-1.06 0 .74.74 0 0 1 0-1.06L13.64 12 8.92 7.28a.74.74 0 0 1 0-1.06.73.73 0 0 1 .53-.22zm4.47 5.72zm0 .57z"></path></svg>
                                        </button>
                                    </Link>
                                </div>
                            </div>
                            </div>
    )
}

export default HotelLayout