import React,{useEffect,useState} from 'react'


const SRpagination = ({place,pagination,setPagination,pagesNum,setPagesNum,currentPage,setCurrentPage,updatedHotelList}) => {

    const [placeName, setPlaceName] = useState()

    useEffect(() => {
        const listLength = updatedHotelList.length
        if(listLength === 0){
            setPagesNum(0)
        }
        else{
            if(listLength%5 === 0){
                const pages = Math.floor(listLength/5)
                setPagesNum(pages)
            }
            else{
                const pages = Math.floor((listLength/5)+1)
                setPagesNum(pages)
            }
        }

    }, [updatedHotelList])

    const handlePaginationClick = (f,s,p) => {
        setPagination({f:f,s:s})
        setCurrentPage(p)
    }
    const handleBackArrow = () =>{
        if(pagination.f!==0){
            setPagination({f:pagination.f-5,s:pagination.s-5})
            setCurrentPage(p=>p-1)
        }
    }

    const handleforwardArrow = () =>{
        if(currentPage!==pagesNum){
            setPagination({f:pagination.f+5,s:pagination.s+5})
            setCurrentPage(p=>p+1)
        }
    }

    const showResults = () => {
        if(currentPage===pagesNum){
            return updatedHotelList.length
        }
        else{
            return pagination.s
        }
    }

    const showResultsBegin = () => {
        if(updatedHotelList.length===0){
            return 0
        }
        else{
            return pagination.f+1
        }
    }


    const handleDisplayVal = (val) => {
        var splitStr = val.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        return splitStr.join(' ')
    }

    useEffect(()=>{
        const placeUpC = handleDisplayVal(place)
        setPlaceName(placeUpC)
    },[updatedHotelList])
    
    return (
        <div className="Results-pagination">
            <div className="text">
                {placeName}: znaleziono {updatedHotelList.length} obiektów
                    </div>
                    <div className="pagination-container">
                        <div className="pages">
                            <button className={currentPage===1 ? "arrow-active" :"arrow"} onClick={handleBackArrow}><svg fill={currentPage===1 ? '#87CEFA': 'black'} className="bk-icon -iconset-navarrow_left bui-pagination__icon"  height="18" role="presentation" width="18" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M73.7 96a4 4 0 0 1-2.9-1.2L40 64l30.8-30.8a4 4 0 0 1 5.7 5.6L51.3 64l25.2 25.2a4 4 0 0 1-2.8 6.8z"></path></svg></button>
                                {[...Array(pagesNum)].map((e, i) => {
                                    return <button  className={currentPage===i+1 ? "page-num__active" : "page-num" } index={e} onClick={()=>handlePaginationClick((i*5),(i*5+5),i+1)}>{i+1}</button>
                                })}
                            <button className={currentPage===pagesNum ? "arrow-active" :"arrow"} onClick={handleforwardArrow}><svg fill={currentPage===pagesNum  ? '#87CEFA': 'black'}  class="bk-icon -iconset-navarrow_right bui-pagination__icon" height="18" role="presentation" width="18" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M54.3 96a4 4 0 0 1-2.8-6.8L76.7 64 51.5 38.8a4 4 0 0 1 5.7-5.6L88 64 57.2 94.8a4 4 0 0 1-2.9 1.2z"></path></svg></button>
                        </div>
                    <div className="results">Wyniki:{" "}
                {showResultsBegin()}-{showResults()}
                </div>
            </div>
        </div>
    )

}

export default SRpagination