import React,{useState,useEffect} from 'react'
import FilterCathSR from '../../elements/filters/filterCathSR'
import FilterSR from '../../elements/filters/filterPriceSR'

const SRfilters = ({filters,checkboxFilterFunc,setFilters,updatedHotelList}) => {


    const [checkedArr,setCheckedArr] = useState([])
    const [checkedPriceArr,setCheckedPriceArr] = useState([])


    const priceArr = [
        { id:1,sP:0,uP:228 },
        { id:2,sP:228,uP:457 },
        { id:3,sP:457,uP:685 },
        { id:4,sP:685,uP: 914 },
        { id:5,sP:914 },
    ]


    useEffect(() => {
        if(checkedArr.length !== 0 || checkedPriceArr.length !== 0){
            handleFilterQuery(checkedPriceArr,checkedArr)
        }
        else{
            handleFilterQuery([],[])
        }
    }, [checkedPriceArr,checkedArr])



    const popularFilters = [
        {id:1,queryName:"BFicluded",name:"Śniadanie wliczone w cenę"},
        {id:2,queryName:"hiddenPool",name:"kryty basen"},
        {id:3,queryName:"above7",name:"Dobry: ponad 7"},
        {id:4,queryName:"privateBath",name:"prywatna łazienka"},
        {id:5,queryName:"parking",name:"parking"},
        {id:6,queryName:"oDbed",name:"Jedno podwójne"},
        {id:7,queryName:"sauna",name:"sauna"},
        {id:8,queryName:"pool",name:"basen"},
    ]

    const handleChecked = (id,cath) => {
        console.log('HC1')
        if(cath=="cath"){
            const currentIndex = checkedArr.indexOf(id)
            const newCheckedArr = [...checkedArr]
            if(currentIndex === -1){
                newCheckedArr.push(id)
            }else{
                newCheckedArr.splice(currentIndex, 1)
            }
            setCheckedArr(newCheckedArr)
        }
        else if(cath="price"){
            const currentIndexPr = checkedPriceArr.indexOf(id)
            const newcheckedPriceArr = [...checkedPriceArr]
            if(currentIndexPr === -1){
                newcheckedPriceArr.push(id)
            }else{
                newcheckedPriceArr.splice(currentIndexPr, 1)
            }
            setCheckedPriceArr(newcheckedPriceArr)
        }
    }

    const handleFilterQuery = (priArr,newArr) => {
        console.log('handlefilter query')
        setFilters({
            ...filters,
            price:[],
            cath:[]
        });
        const ArrPr = []
            for(let j=0;j<priArr.length;j++){
                for(let i=0;i<priceArr.length;i++){
                    if(priceArr[i].id === priArr[j]){
                        ArrPr.push([priceArr[i].sP, priceArr[i].uP])
                    }
                }
            }

        
        const Arr = [];
            for(let j=0;j<newArr.length;j++){
                for(let i=0;i<popularFilters.length;i++){
                    if(popularFilters[i].id === newArr[j]){
                        Arr.push(popularFilters[i].queryName)
                    }
                }
            }

        setFilters({
            ...filters,
            price:ArrPr,
            cath:Arr
        })

        // checkboxFilterFunc(Arr,ArrPr)
    }


    return (
        <div className="filters">
                        <div className="filters-title">
                            Filtruj według następujących kryteriów: 
                        </div>
                        <div className="filter-text">
                            <div className="filter-text__title">
                                Twój przedział cenowy (za noc)
                            </div>
                            {priceArr.map(el => {

                                return (
                                    <FilterSR 
                                    handleChecked={filters => handleChecked(filters,"price")} 
                                    el={el}
                                    updatedHotelList={updatedHotelList}
                                    />
                                )})}
                        </div>
                        <div className="filter-text">
                            <div className="filter-text__title">
                                Popularne filtry
                            </div>
                            {popularFilters.map(el => {

                                    return(

                                        <FilterCathSR
                                        handleChecked={filters => handleChecked(filters,"cath")}
                                        el={el}
                                        updatedHotelList={updatedHotelList}
                                        />
                            )})}
                        </div>
                    </div>
    )
}

export default SRfilters

