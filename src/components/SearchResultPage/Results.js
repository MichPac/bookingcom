import React,{useEffect, useState} from 'react';
import { useStaticQuery, graphql} from "gatsby";
import HotelLayout from "./HotelLayout";
import SearchFormSR from './SearchFormSR';
import CurrentDirectory from './CurrentDirectory';
import SRfilters from './SRfilters';
import SRpagination from './SRpagination';
import SRuppperFilterBar from './SRupperFilterBar';
import {CountriesList,regionsList,placesList} from '../../data/placesForFiltering';



const Results = (props) => {


    const data = useStaticQuery(graphql`
    query{
         allObjectsSJson {
                nodes {
                id
                img
                odcentrum
                opinion
                opinions
                partnerProgram
                country
                region
                path
                place
                price
                score
                name
                BFicluded
                hiddenPool
                above7
                privateBath
                parking
                oDbed
                sauna
                pool
                wholeHA
                commission
                }
            }
        }
    `)

    const hotelList = data.allObjectsSJson.nodes
    const [updatedHotelList, setUpdatedHotelList] = useState(hotelList)
    const [orginalList,setOrginalList] = useState([])

    const [filters,setFilters] = useState({
        price:[],
        cath:[],
        sortby:'chsnBU'
    })

    /* lowerPrice,BOPrice */

    const[placeState,setPlaceState]= useState(props.searchData.place)

    const placeDataNormalize = props.searchData.place.toLowerCase().trim()
    const place = placeDataNormalize.normalize('NFD').replace(/[\u0300-\u036f]/g, "")

        /* pagination states */

    const[pagination,setPagination] = useState({f:0,s:5})
    const[pagesNum,setPagesNum] = useState()
    const[currentPage,setCurrentPage] = useState(1)

    useEffect(()=>{
        setPlaceState(place)
    },[props.searchData.place])

    useEffect(() => {
        for(let i=0;i<CountriesList.length;i++){
            if(place===CountriesList[i]){
                filterFunc(place,"country")
            }
        }
        for(let i=0;i<regionsList.length;i++){
            if(place===regionsList[i]){
                filterFunc(place,"region")
            }
        }
        for(let i=0;i<placesList.length;i++){
            if(place===placesList[i]){
                filterFunc(place,"place")
            }
        }
    }, [props.searchData])

    const filterFunc = (val,parameter) => {
        const filteredList = [...hotelList].filter(el => el[parameter] == val)
        const sortedByComission = [...filteredList].sort((a, b) => (a.commission > b.commission) ? -1 : 1)
        setUpdatedHotelList(sortedByComission)
        setOrginalList(filteredList)

    }


    const paginationRestart = () => {
        setPagination({f:0,s:5})
        setCurrentPage(1)
    }

    const removeDuplicates = (data,key) => {
        return [
            ...new Map(data.map(item => [key(item), item])).values()
        ]
    }

    const [asecurateArr, setAsecurateArr] = useState([])

    useEffect(() => {
        if(asecurateArr.length !== 0){
            setUpdatedHotelList(asecurateArr)
        }
    }, [asecurateArr])


    useEffect(()=>{
        checkboxFilterFunc(filters.cath,filters.price)
    },[filters])
    
    const checkboxFilterFunc = (filtersArr,filtersPriceArr) => {
        
        const filteredArr = []
        const helpArr = []
        if(filtersArr.length === 0 && filtersPriceArr.length === 0){
            const sortedArr = sortByCath([...orginalList],filters.sortby)
            setUpdatedHotelList(sortedArr)
        }
        else if(filtersArr.length !== 0 && filtersPriceArr.length === 0){
            for(let i=0;i<=filtersArr.length;i++){
                const filteredCathList = [...orginalList].filter(el => el[filtersArr[i]] === true)
                filteredArr.push(...filteredCathList)
            }
            const removeCathDuplicates = removeDuplicates(filteredArr, el => el.id)
            const sortedArr = sortByCath([...removeCathDuplicates],filters.sortby)
            setUpdatedHotelList(sortedArr)
        }


        else if(filtersArr.length === 0 && filtersPriceArr.length !== 0){
            for(let i=0;i<filtersPriceArr.length;i++){
                const filteredPrList = [...orginalList].filter(el => el.price >= filtersPriceArr[i][0] && el.price < filtersPriceArr[i][1])
                filteredArr.push(...filteredPrList) 
            }
            const sortedArr = sortByCath([...filteredArr],filters.sortby)
            setUpdatedHotelList(sortedArr)
        }
        else{
            for(let i=0;i<filtersPriceArr.length;i++){
                const filteredPrList = [...orginalList].filter(el => el.price >= filtersPriceArr[i][0] && el.price < filtersPriceArr[i][1])
                filteredArr.push(...filteredPrList) 
            }
            setAsecurateArr(filteredArr)

            for(let i=0;i<=filtersArr.length;i++){
                const filteredCathList = [...filteredArr].filter(el => el[filtersArr[i]] === true)
                helpArr.push(...filteredCathList)
            }
            const removeAllDuplicates = removeDuplicates(helpArr, el => el.id)

            if(removeAllDuplicates.length === 0){
                setUpdatedHotelList([])
            }
            const sortedArr = sortByCath([...removeAllDuplicates],filters.sortby)
            setAsecurateArr(sortedArr)
        }

        paginationRestart()

    }

    const sortByCath = (Arr,sortByVal) => {
        
        if(sortByVal==='chsnBU'){
            const sortedByComission = [...Arr].sort((a, b) => (a.commission > b.commission) ? -1 : 1)
            console.log('essa1')
            console.log(Arr)
            console.log(sortedByComission)
            console.log('essa1')
            return sortedByComission
        }
        else if(sortByVal==='lowstPri'){
            const orderedList = [...Arr].sort((a, b) => (a.price > b.price) ? 1 : -1);
            console.log('essa2')
            console.log( orderedList)
            console.log('essa2')
            return orderedList
        }
        else if(sortByVal==='BscrPri'){
            const orderedList = [...Arr].sort((a, b) => (a.score > b.score) ? -1 : 1  && (a.price > b.price) ? 1 : -1);
            console.log('essa3')
            console.log(orderedList)
            console.log('essa3')
            return orderedList
        }

    }


  return(
      
        <React.Fragment>
            <div className="SearchResult">
                <CurrentDirectory 
                    place={placeState} 
                    showLandingPage={props.showLandingPage}
                />
                <div className="Results">
                    <div className="searchForm">
                        <SearchFormSR {...props}/>
                        <SRfilters 
                            checkboxFilterFunc={checkboxFilterFunc}
                            filters={filters}
                            setFilters={setFilters}
                            updatedHotelList={updatedHotelList}
                        />
                    </div>
                    <div className="Results-list">
                        <SRuppperFilterBar
                            updatedHotelList={updatedHotelList}
                            setFilters={setFilters}
                            filters={filters}
                            place={props.searchData.place}
                        />
                        <div className="Results-hotels">
                            <div className="Results-hotels__list">
                                {updatedHotelList.slice(pagination.f,pagination.s).map(post => (
                                  <HotelLayout {...post}
                                  />
                                ))}

                            </div>
                        </div>
                        <SRpagination
                            place={place}
                            pagination={pagination}
                            setPagination={setPagination}
                            pagesNum={pagesNum}
                            setPagesNum={setPagesNum}
                            currentPage={currentPage}
                            setCurrentPage={setCurrentPage}
                            updatedHotelList={updatedHotelList}
                        />
                    </div>
                </div>
            </div>
        </React.Fragment>
  )
}




export default Results
