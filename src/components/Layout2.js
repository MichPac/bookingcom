 import * as React from "react"
 import Header from '../components/LayoutsComp/Header2'
 import Footer from "../components/LayoutsComp/Footer"
 import "./layout.css"
import Biuletyn from "./LayoutsComp/Biuletyn"
import ShareObj from './LayoutsComp/ShareObj'
import FooterList from './LayoutsComp/FooterList'
 
 const Layout2 = (props) => {

 
   return (
     <>
       <Header/>
        {props.children}
        <Biuletyn/>
        <ShareObj/>
        <FooterList/>
       <Footer/>
     </>
   )
 }

 export default Layout2