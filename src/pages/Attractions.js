import React from 'react'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Link} from 'gatsby';
import Layout from '../components/Layout1';
import UpContentA from '../components/AttractionsPage/UpContentA';
import MiddleContentA from '../components/AttractionsPage/MiddleContentA';
import DownContentA2 from '../components/AttractionsPage/DownContentA2';
import SearchBarA from '../components/AttractionsPage/SearchBarA';


const AttractionsPage = () => {

    
    return (
        <Layout>
                <SearchBarA/>
                <UpContentA/>
                <MiddleContentA/>
                <DownContentA2/>
              <div className="subfoter">
                  <div className="subfooter-list">
                      <div className="subfooter-list__el"><Link className='link' to="/">O Booking.com</Link></div>
                      <div className="subfooter-list__el"><Link className='link' to="/">Warunki</Link></div>
                      <div className="subfooter-list__el"><Link className='link'  to="/">Oświadczenie o ochronie prywatności i plikach cookie</Link></div>
                      <div className="subfooter-list__el"><Link className='link' to="/">Pomoc w zakresie lotów</Link></div>
                  </div>
              </div>
        </Layout>
        
    )
}




export default AttractionsPage