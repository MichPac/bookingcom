import React,{useState} from "react"
import MainSection from '../components/Homepage/MainSection'
import SearchBarH from '../components/Homepage/SearchBarH'
import Layout from '../components/Layout2'
import "../styles/default/theme.scss";
import Results from '../components/SearchResultPage/Results'



const  IndexPage = () => {



    const[searchResult,setSearchResult] = useState(true)
    const[searchData,setSearchData] = useState(
        {
            place:'',
            adults: 1,
            kids: 0,
            rooms: 1,
            date1: '',
            date2: ''
        }
    )

    const handleChange = (e) => {
        setSearchData({...searchData,place:e.target.value})
    }

    const changeVal = (val,par) => {
        if(searchData[par]>1 || val!==-1){
            setSearchData({...searchData,[par]:searchData[par]+val})
        }
        else if(par==='kids' && searchData[par]>0 || val !== -1){
            setSearchData({...searchData,[par]:searchData[par]+val})
        }
 
    }

    const showLandingPage = () => {
        setSearchData(        {
            place:'',
            adults: 1,
            kids: 0,
            rooms: 1,
            date1: '',
            date2: ''
        })
         setSearchResult(true)
    }


    return (
        <React.Fragment>

            <div>
            { searchResult ?
                <Layout>
                  <SearchBarH
                    searchData={searchData} 
                    setSearchData={setSearchData}
                    handleChange={handleChange}
                    setSearchResult={setSearchResult}
                    changeVal={changeVal}
                    />
                  <MainSection/>
                </Layout> :
                <Layout {...{showLandingPage}}>            
                   <Results 
                   {...{showLandingPage,searchResult,setSearchResult,searchData,setSearchData}}/>
                </Layout>
            }
            </div>
        </React.Fragment>
    )
}

export default IndexPage