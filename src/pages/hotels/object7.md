---
path: '/apartamentyszklarska'
odcentrum: "0,2"
name: "Apartamenty Szklarska"
place: "Szklarska Poręba"
opinions: "60"
opinion: "Znakomity"
score: "9,5"
price: 120
partnerProgram: "true"
img: "https://cf.bstatic.com/xdata/images/hotel/square200/4862098.webp?k=7b946ed30bafa72306fc8eb7d7d8e51a492da4b61fa93146ca0bea2701b81669&o="
desc: "Obiekt Willa Hektor, położony w miejscowości Szklarska Poręba, oferuje wspólny salon, ogród oraz różne opcje zakwaterowania, w których zapewniono bezpłatne WiFi i telewizor z płaskim ekranem."
---

