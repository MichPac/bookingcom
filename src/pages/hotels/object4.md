---
path: '/pokojegoscinneklaudia'
odcentrum: "0,8"
name: "Pokoje gościnne Klaudia"
place: "Szklarska Poręba"
opinions: "343"
opinion: "Znakomity"
score: "9,2"
price: 230
img: "https://cf.bstatic.com/xdata/images/hotel/square200/163523979.webp?k=26f26ffd01515227acb5d25de14d67841568d9bbfedd66a7c2227ef232a61b30&o="
desc: "Obiekt Pokoje Gościnne Claudia położony jest w miejscowości Szklarska Poręba w regionie dolnośląskie i zapewnia bezpłatne WiFi oraz bezpłatny prywatny parking. "
---

