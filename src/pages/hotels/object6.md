---
path: '/hotelwiktoria'
odcentrum: "0,4"
name: "Hotel Wiktoria"
place: "Szklarska Poręba"
opinions: "373"
opinion: "Dobry"
score: "7,6"
price: 135
partnerProgram: "true"
img: "https://cf.bstatic.com/xdata/images/hotel/square200/258860232.webp?k=c4a3ba7a4539b9bb1cb32d714b7134499d0e34509c02fb88d2a33046e2ab296e&o="
desc: "Obiekt Willa Hektor, położony w miejscowości Szklarska Poręba, oferuje wspólny salon, ogród oraz różne opcje zakwaterowania, w których zapewniono bezpłatne WiFi i telewizor z płaskim ekranem."
---



