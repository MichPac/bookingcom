---
path: '/willagencjana'
odcentrum: "0,9"
name: "willa Gencjana"
place: "Szklarska Poręba"
opinions: "82"
opinion: "Cudowny"
score: "8,8"
price: 135
img: "https://cf.bstatic.com/xdata/images/hotel/square200/8543805.webp?k=bb211d1217728833661f9b9500656f5eb592a0bedeeb8ca87bfe5edde2266839&o="
desc: "Obiekt Willa Hektor, położony w miejscowości Szklarska Poręba, oferuje wspólny salon, ogród oraz różne opcje zakwaterowania, w których zapewniono bezpłatne WiFi i telewizor z płaskim ekranem."
---


