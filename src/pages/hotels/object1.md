---
path: '/villamarta'
odcentrum: "0,6"
name: "Villa Marta"
place: "Szklarska Poręba"
opinions: "246"
opinion: "Bardzo dobry"
score: "8,3"
price: 100
partnerProgram: "true"
img: "https://cf.bstatic.com/xdata/images/hotel/square200/256443906.webp?k=4aa82a072c93c7c3124e9461a43dea0d25ec7d7e9765bd4d56cdde14765fe0c3&o="
desc: "Dom Gościnny Czajka położony jest w zacisznej okolicy w kurorcie Szklarska Poręba i oferuje pokoje i apartamenty z pięknym widokiem na Karkonosze."
---
