---
path: '/zabukiem'
odcentrum: "0,8"
name: "Za bukiem"
place: "Szklarska Poręba"
opinions: "207"
opinion: "Fantastyczny"
score: "8,9"
price: 160
img: "https://cf.bstatic.com/xdata/images/hotel/square200/37843863.webp?k=bccc7b0dac78bad5db82fbdcf2b615b71ed7c444f33640cb0f94516ea4a7fd1b&o="
desc: "Obiekt Willa Hektor, położony w miejscowości Szklarska Poręba, oferuje wspólny salon, ogród oraz różne opcje zakwaterowania, w których zapewniono bezpłatne WiFi i telewizor z płaskim ekranem."
---



