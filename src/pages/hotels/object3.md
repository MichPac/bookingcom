---
path: '/willahektor'
odcentrum: "0,7"
name: "Willa Hektor"
place: "Szklarska Poręba"
opinions: "400"
opinion: "Znakomity"
score: "9,2"
price: 230
partnerProgram: "true"
img: "https://cf.bstatic.com/xdata/images/hotel/square200/278124515.webp?k=1e36b08f6d513fb3a65e44702ff76a847c63c8f658a0f6a8eeaa4dc613f762e9&o="
desc: "Dom Gościnny Czajka położony jest w zacisznej okolicy w kurorcie Szklarska Poręba i oferuje pokoje i apartamenty z pięknym widokiem na Karkonosze."
---
