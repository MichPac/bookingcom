---
path: '/czajka'
odcentrum: "1,2"
name: "Czajka"
place: "Szklarska Poręba"
opinions: "246"
opinion: "Bardzo dobry"
score: "8,2"
price: 120
img: "https://cf.bstatic.com/xdata/images/hotel/square200/100836562.webp?k=5f8ffb221167f9e0bc46c9a8c055a14e28a2b86fd000a423f4f1dd8fcd09ab7f&o="
desc: "Dom Gościnny Czajka położony jest w zacisznej okolicy w kurorcie Szklarska Poręba i oferuje pokoje i apartamenty z pięknym widokiem na Karkonosze."
---
