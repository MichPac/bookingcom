import Layout from '../components/Layout2';
import DownerContentAT from '../components/AirPortTaxiPage/DownerContentAT';
import MiddleContentAT from '../components/AirPortTaxiPage/MiddleContentAT';
import SearchBarAT from '../components/AirPortTaxiPage/SearchBarAT';
import DownContentAT from '../components/AirPortTaxiPage/DownContentAT';
import ReadmoreAT from '../components/AirPortTaxiPage/ReadmoreAT';
import React from 'react';

const AirportTaxiPage = () => {
    
    return (
        <Layout>
              <div className="searchBarAT"> 
                  <div className="searchBarAT-bar__container">
                    <SearchBarAT/>
                  </div>
              </div>
              <DownerContentAT/>
              <MiddleContentAT/>
              <DownContentAT/>
              <ReadmoreAT/>
        </Layout>
        
    )
}




export default AirportTaxiPage


