import React from 'react'
import UpperContentCR from '../components/CarRentalPage/UpperContentCR'
import SearchFormCR from '../components/CarRentalPage/SearchFormCR'
import Layout from '../components/Layout2'
import UpContentCR from '../components/CarRentalPage/UpContentCR'
import MiddleContentCR from '../components/CarRentalPage/MiddleContentCR'
import DownContentCR from '../components/CarRentalPage/DownContentCR'
import DownerContentCR from '../components/CarRentalPage/DownerContentCR'


const  CarRentalPage = () => {
    return (
        <Layout>
            <SearchFormCR/>
            <UpperContentCR/>
            <UpContentCR/>
            <MiddleContentCR/>
            <DownContentCR/>
            <DownerContentCR/>
        </Layout>
    )
}

export default CarRentalPage