import React from 'react'
import Layout from '../components/Layout1'
import DownContentF from '../components/SearchFlightsPage/DownContentF'
import SearchFormF from '../components/SearchFlightsPage/SearchFormF'



const Flights = () => {
    return (
        <Layout>
            <SearchFormF/>
            <DownContentF/>
        </Layout>
    )
}

export default Flights