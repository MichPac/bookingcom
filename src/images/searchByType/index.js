import h1 from './h1.webp';
import h2 from './h2.jpeg';
import h3 from './h3.jpeg';
import h4 from './h4.jpeg';
import h5 from './h5.jpeg';
import h6 from './h6.jpeg';
import h7 from './h7.jpeg';
import h8 from './h8.jpeg';
import h9 from './h9.jpeg';
import h10 from './h10.jpeg';


const hotelsByCath = {
    h1,h2,h3,h4,h5,h6,h7,h8,h9,h10
}

export default hotelsByCath