import React,{useState,useEffect} from "react";
import useComponentVisible from "../../functions/UseComponenetVisibleHook";
import Calendar  from 'react-calendar'

const  HCalendar = ({searchData,setSearchData}) => {

    const { ref,isComponentVisible,setIsComponentVisible } = useComponentVisible(false);
    const[visible,setVisible] = useState(false)

    const onChange = (e) => {
        setSearchData({
            ...searchData,
            date1:e[0],
            date2:e[1]
        })
        setVisible(false)

    }

    const weekDaysArr = ['pn','wt','śr','czw','pt','sob','nd']
    const monthArr = ['STY','LUT','MAR','KWI','MAJ','CZE','LIP','SIE','WRZ','PAŹ','LIS','GRU']

    const displayVal = () => {

        if(searchData.date1 === ''){
            return "Zameldowanie - Wymeldowanie"
        }
        else if(searchData.date2 !== ''){
            const date1 = searchData.date1
            const weekDay1 = date1.getDay()
            const monthDay1 = date1.getUTCDate()+1
            const month1 = date1.getMonth()
            const newWD1 = weekDaysArr[weekDay1]
            const newM1 = monthArr[month1]
            
            const date2 = searchData.date2
            const weekDay2 = date2.getDay()
            const monthDay2 = date2.getUTCDate()+1
            const month2 = date2.getMonth()
            const newWD2 = weekDaysArr[weekDay2]
            const newM2 = monthArr[month2]
            return newWD1+" "+monthDay1+" "+newM1+" - "+newWD2+" "+monthDay2+" "+newM2
        }
    }

    return(
            <div ref={ref} onClick={() => {setIsComponentVisible(true);setVisible(true)}} className="date">
                <div className="date-data">
                    <svg viewBox="0 0 24 24" fill="#BDBDBD" width="25" height="25"><path d="M22.502 13.5v8.25a.75.75 0 01-.75.75h-19.5a.75.75 0 01-.75-.75V5.25a.75.75 0 01.75-.75h19.5a.75.75 0 01.75.75v8.25zm1.5 0V5.25A2.25 2.25 0 0021.752 3h-19.5a2.25 2.25 0 00-2.25 2.25v16.5A2.25 2.25 0 002.252 24h19.5a2.25 2.25 0 002.25-2.25V13.5zm-23.25-3h22.5a.75.75 0 000-1.5H.752a.75.75 0 000 1.5zM7.502 6V.75a.75.75 0 00-1.5 0V6a.75.75 0 001.5 0zm10.5 0V.75a.75.75 0 00-1.5 0V6a.75.75 0 001.5 0z"></path></svg>
                    {displayVal()}
                    {isComponentVisible && visible &&
                        <div className="date-calendar">
                            <Calendar
                                returnValue={"range"}
                                value={[searchData.date1,searchData.date2]}
                                selectRange={true}
                                showDoubleView={true}
                                onChange={onChange}
                                showNeighboringMonth={false}
                            />
                        </div>
                    }
                </div>          
            </div>
            )
}

export default HCalendar


// tileContent={
//     ({ activeStartDate, date, view }) => {
//       return view === 'month' && date.getDay() === 0 
//       ? <p onMouseHover={
//           //do whatever you want
//           console.log('hi')
//           }>It's Sunday!</p> 
//       : null
//     }
//   }