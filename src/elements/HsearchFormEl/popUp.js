import React from 'react'
import useComponentVisible from "../../functions/UseComponenetVisibleHook";


const PopUp = ({changeVal,searchData}) => {

const { ref,isComponentVisible,setIsComponentVisible } = useComponentVisible(false);

    const handleDisplayAdl = () => {
        if(searchData.adults>1){
            return 'dorosłych'
        }
        else{
            return 'dorosły'
        }
    }

    const handleDisplayRms = () => {
        if(searchData.rooms===1){
            return 'pokój'
        }
        else if(searchData.rooms<5){
            return 'pokoje'
        }
        else{
            return 'pokoi'
        }
    }


    return (
        <div ref={ref} className="people">
            <div onClick={() => setIsComponentVisible(true)} className="people-num">
                <span className="person-icon"><svg width="24px" height="24px" fill="lightgrey" class="rw-icon gb-o-interactive-field__icon" viewBox="0 0 128 128" version="1.1" xmlns="http://www.w3.org/2000/svg" focusable="false"><path d="M104 120H24a8 8 0 0 1-8-8V93.5a4 4 0 0 1 1-2.7C21.3 86.4 36.9 72 64 72s42.8 14.4 47 18.8a4 4 0 0 1 1 2.7V112a8 8 0 0 1-8 8zM64 8a28 28 0 1 0 28 28A28 28 0 0 0 64 8z" fill-rule="evenodd"></path></svg></span>
                <span>{searchData.adults} {handleDisplayAdl()}</span>
                <span>&nbsp;·&nbsp;</span>
                <span>{searchData.kids} dzieci</span>
                <span>&nbsp;·&nbsp;</span>
                <span>{searchData.rooms} {handleDisplayRms()}</span>
                <span className="arrows">
                    <span className="arrow-icon2"><svg xmlns="http://www.w3.org/2000/svg" fill="#0071c2" width="10" height="10" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"/></svg></span>
                    <span className="arrow-icon"><svg xmlns="http://www.w3.org/2000/svg" fill="#0071c2" width="10" height="10" viewBox="0 0 24 24"><path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z"/></svg></span>
                </span>
            </div>
            {isComponentVisible && 
                <div className="people-popUp">
                    <div className="people-popUp__container">
                        <div className="popUp-row">
                            <div className="text">Dorośli</div>
                            <div className="values">
                                <div onClick={()=>changeVal(-1,"adults")} className={`minus ${searchData.adults===1 ? "btn": ""}`}>-</div>
                                <div className="val">{searchData.adults}</div>
                                <div onClick={()=>changeVal(1,"adults")} className={`plus ${searchData.adults===30 ? "btn": ""}`}>+</div>
                            </div>
                        </div>
                        <div className="popUp-row">
                            <div className="text">
                            Dzieci
                            <span className="span">w wieku od 0 do 17 lat</span>
                            </div>
                            <div className="values">
                                <div onClick={()=>changeVal(-1,"kids")} className={`minus ${searchData.kids===0 ? "btn": ""}`}>-</div>
                                <div className="val">{searchData.kids}</div>
                                <div onClick={()=>changeVal(1,"kids")} className={`plus ${searchData.kids===10 ? "btn": ""}`}>+</div>
                            </div>
                        </div>
                        <div className="popUp-row">
                            <div className="text">Pokoje</div>
                            <div className="values">
                                <div  onClick={()=>changeVal(-1,"rooms")} className={`minus ${searchData.rooms===1 ? "btn": ""}`}>-</div>
                                <div className="val">{searchData.rooms}</div>
                                <div onClick={()=>changeVal(1,"rooms")} className={`plus ${searchData.rooms===30 ? "btn": ""}`}>+</div>
                            </div>
                        </div>
                    </div>
                </div>
            }
            
        </div>
    )
}

export default PopUp