import React,{useState,useEffect} from "react";
import useComponentVisible from "../../functions/UseComponenetVisibleHook";
import Calendar  from 'react-calendar'

const  FCalendar = ({calendarPar,inputData,setInputData}) => {

    const { ref,isComponentVisible,setIsComponentVisible } = useComponentVisible(false);
    const[visible,setVisible] = useState(false)


    const onChange = (e) => {
        if(calendarPar.returnVal==="range"){
            setInputData({
                ...inputData,
                date: e[0],
                date2: e[1]
            })
        }
        else if(calendarPar.returnVal==="start"){
            
            setInputData({...inputData,date:e})
            console.log(e)
        }
        setVisible(false)

    }

    const weekDaysArr = ['pn','wt','śr','czw','pt','sob','nd']
    const monthArr = ['STY','LUT','MAR','KWI','MAJ','CZE','LIP','SIE','WRZ','PAŹ','LIS','GRU']

    const displayVal = () => {

        if(inputData.date === ''){
            return "Kiedy?"
        }
        else if(inputData.date2 !== ''){
                const date1 = inputData.date
                const weekDay1 = date1.getDay()
                const monthDay1 = date1.getUTCDate()+1
                const month1 = date1.getMonth()
                const newWD1 = weekDaysArr[weekDay1]
                const newM1 = monthArr[month1]
                const date2 = inputData.date2
                const weekDay2 = date2.getDay()
                const monthDay2 = date2.getUTCDate()
                const month2 = date2.getMonth()
                const newWD2 = weekDaysArr[weekDay2]
                const newM2 = monthArr[month2]
            
            
            return newWD1+" "+monthDay1+" "+newM1+" - "+ newWD2+" "+monthDay2+" "+newM2
        }
        else{
            const date1 = inputData.date
            const weekDay1 = date1.getDay()
            const monthDay1 = date1.getUTCDate()+1
            const month1 = date1.getMonth()
            const newWD1 = weekDaysArr[weekDay1]
            const newM1 = monthArr[month1]

            return newWD1+" "+monthDay1+" "+newM1
        }
    }



    return(
            <div ref={ref} onClick={() => {setIsComponentVisible(true);setVisible(true)}} className="input when">
                <svg viewBox="0 0 24 24" fill="#BDBDBD" width="30" height="30"><path d="M22.502 13.5v8.25a.75.75 0 01-.75.75h-19.5a.75.75 0 01-.75-.75V5.25a.75.75 0 01.75-.75h19.5a.75.75 0 01.75.75v8.25zm1.5 0V5.25A2.25 2.25 0 0021.752 3h-19.5a2.25 2.25 0 00-2.25 2.25v16.5A2.25 2.25 0 002.252 24h19.5a2.25 2.25 0 002.25-2.25V13.5zm-23.25-3h22.5a.75.75 0 000-1.5H.752a.75.75 0 000 1.5zM7.502 6V.75a.75.75 0 00-1.5 0V6a.75.75 0 001.5 0zm10.5 0V.75a.75.75 0 00-1.5 0V6a.75.75 0 001.5 0z"></path></svg>
                    {displayVal()}
                    {isComponentVisible && visible && 
                    <div className="date-calendar">
                        <Calendar
                            returnValue={calendarPar.returnVal}
                            value={[inputData.date,inputData.date2]}
                            defaultValue={[inputData.date,inputData.date2]}
                            selectRange={calendarPar.selectRange}
                            showDoubleView={true}
                            onChange={onChange}
                            showNeighboringMonth={false}
                        />
                    </div>
                    }
                </div>
           

    )
}

export default FCalendar

