import React,{useState,useRef,useEffect} from 'react'
import useComponentVisible from "../../functions/UseComponenetVisibleHook";


const PopUp = () => {

    const[people,setPeople] = useState({
        adults: 1,
        kids:0
    })

    const { ref,isComponentVisible,setIsComponentVisible } = useComponentVisible(false);

    const changeVal = (val,par) => {
        if(people[par]>1 || val !== -1){
            setPeople({...people,[par]:people[par]+val})
        }
        else if(par==='kids' && people[par]>0 || val !== -1){
            setPeople({...people,[par]:people[par]+val})
        }
        
    }
    
    const displayVal = () =>{
        if(people.adults===1 && people.kids===0){
            return people.adults+" dorosły"
        }
        else if(people.adults>1 && people.kids===0){
            return people.adults+" dorosłych"
        }
        else{
            return people.adults+people.kids+" podrózujących"
        }
    }

    return (
        <div ref={ref} className="people">
            <div onClick={() => setIsComponentVisible(true)} className="people-container">
                <div className="people-val">{displayVal()}</div>
                <div className="icon">
                    <svg viewBox="0 0 24 24" fill="#333333" width="24px" height="24px"><path d="M7 10.12l5 5 5-5H7z"></path></svg>
                </div>
            </div>
            {isComponentVisible &&
                <div className="PopUp">
                    <div className="window">
                        <div className="desc">
                            <div className="title">Dorośli</div>
                            <div className="subtitle">w wieku powyzej 18 lat</div>
                        </div>
                        <div className="numbers">
                            <div 
                            className={`minus ${people.adults===1 ? "btn-notactive" : "btn"}`} onClick={()=>changeVal(-1,'adults')}>-</div>
                            <div className="value">{people.adults}</div>
                            <div  className="plus btn" onClick={()=>changeVal(1,'adults')}>+</div>
                        </div>
                    </div>
                    <div className="window kids">
                        <div className="desc">
                            <div className="title">Dzieci</div>
                            <div className="subtitle">w wieku od 0 do 17 lat</div>
                        </div>
                        <div className="numbers">
                            <div className={`minus ${people.kids===0 ? "btn-notactive" : "btn"}`} onClick={()=>changeVal(-1,'kids')}>-</div>
                            <div className="value">{people.kids}</div>
                            <div className="plus btn" onClick={()=>changeVal(1,'kids')}>+</div>
                        </div>
                    </div>
                    <div className="submit-container">
                        <div className="val">
                            <div className="title">{displayVal()}</div>
                        </div>
                        <div className="submit-btn">
                            <button onClick={() => setIsComponentVisible(v=>!v)}>Gotowe</button>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}

export default PopUp