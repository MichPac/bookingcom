import React,{useEffect, useState} from 'react'


const Select = ({handleChange,inputData}) => {

    const[width,setWidth]= useState(inputData.class.length)

    useEffect(()=>{
        const newWidth = inputData.class.length
        console.log(newWidth)
        if(newWidth===19){
            setWidth(218)
        }
        else if(newWidth===11){
            setWidth(139)
        }
        else if(newWidth===9){
            setWidth(117)
        }
        else if(newWidth===14){
            setWidth(150)
        }
    },[inputData])

    return (
        <select onChange={(e)=>handleChange(e)}
            name="class" style={{width:`${width}px`}}
            value={inputData.class} name="class" id="class">
                <option value="Ekonomiczna">Ekonomiczna</option>
                <option value="Ekonomiczna premium">Ekonomiczna premium</option>
                <option value="Biznesowa">Biznesowa</option>
                <option value="Pierwsza klasa">Pierwsza klasa</option>
        </select>
    )

}

export default Select