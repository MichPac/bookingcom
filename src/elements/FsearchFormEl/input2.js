import React from 'react'
import useComponentVisible from "../../functions/UseComponenetVisibleHook";


const Input2 = () => {

    const { ref,isComponentVisible,setIsComponentVisible } = useComponentVisible(false);
    
    return (
        <div ref={ref} className="input from-where">
            <div onClick={() => setIsComponentVisible(true)}  className="text">
                <svg className='svg' viewBox="0 0 24 24" fill="#0071C2" width="30" height="30"><path d="M10.5 15.75h12.75a.75.75 0 000-1.5H10.5a.75.75 0 000 1.5zm9.22-3.22l3 3v-1.06l-3 3a.75.75 0 101.06 1.06l3-3a.75.75 0 000-1.06l-3-3a.75.75 0 10-1.06 1.06zM13.5 8.25H.75a.75.75 0 000 1.5H13.5a.75.75 0 000-1.5zm-9.22 3.22l-3-3v1.06l3-3a.75.75 0 00-1.06-1.06l-3 3a.75.75 0 000 1.06l3 3a.75.75 0 001.06-1.06z"></path></svg>
                <div className="text">Dokąd Lecisz?</div>
            </div>
            {isComponentVisible && (
                <div className="popUp">
                    <input placeholder="Dokąd lecisz?"></input>
                </div>
            )}
        </div>               
    )
}

export default Input2
