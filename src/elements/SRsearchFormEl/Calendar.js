import React,{useState,useEffect} from "react";
import useComponentVisible from "../../functions/UseComponenetVisibleHook";
import Calendar  from 'react-calendar'
import Calendar2 from './Calendar2'

const  CRCalendar = ({inputData,setInputData}) => {


    const { ref,isComponentVisible,setIsComponentVisible } = useComponentVisible(false);
    const [visible,setVisible] = useState('cal1')

    const weekDaysArr = ['poniedziałek','wtorek','środa','czwartek','piątek','sobota','niedziela']
    const monthArr = ['Styczeń', 'Luty','Marzec' ,'Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Listopad','Grudzień']

    const displayRightFormat = (val,par) => {
    if(val){
        const day = val.getDay()
        const newDay = weekDaysArr[day]
        let monthDay = val.getUTCDate()
        const month = val.getMonth()
        const monthName = monthArr[month]

        return newDay+" "+monthDay+" "+monthName
    }
    else if(!val && par==='date1'){
        return 'Data zameldowania'
    }
    else if(!val && par==='date2')
        return 'Data wymeldowania'
    }

    const onChange = (e) => {
            setInputData({
                ...inputData,
                date1: e[0],
                date2: e[1]
            })
            console.log(e)
            setVisible(false)
    }

    const displayMonthDay = (val) => {
        if(val){
            const monthDay = val.getUTCDate()
            return monthDay
        }
    }

        
    return(
        <div ref={ref}>
                    <div className="form-input">
                        <div onClick={()=>{setIsComponentVisible(true);setVisible('cal1')}}>
                             <label>Od</label>
                            <div className="date">
                                <div className="UTCdays">
                                    <div className="val">
                                        {displayMonthDay(inputData.date1)}
                                    </div>
                                </div>
                                <div className="svg1">
                                    <svg aria-hidden="true" class="bk-icon -streamline-calendar sb-date-picker_icon-svg" fill="black" focusable="false" height="22" role="presentation" width="22" viewBox="0 0 24 24"><path d="M22.502 13.5v8.25a.75.75 0 0 1-.75.75h-19.5a.75.75 0 0 1-.75-.75V5.25a.75.75 0 0 1 .75-.75h19.5a.75.75 0 0 1 .75.75v8.25zm1.5 0V5.25A2.25 2.25 0 0 0 21.752 3h-19.5a2.25 2.25 0 0 0-2.25 2.25v16.5A2.25 2.25 0 0 0 2.252 24h19.5a2.25 2.25 0 0 0 2.25-2.25V13.5zm-23.25-3h22.5a.75.75 0 0 0 0-1.5H.752a.75.75 0 0 0 0 1.5zM7.502 6V.75a.75.75 0 0 0-1.5 0V6a.75.75 0 0 0 1.5 0zm10.5 0V.75a.75.75 0 0 0-1.5 0V6a.75.75 0 0 0 1.5 0z"></path></svg>
                                </div>
                                <div className="text">
                                    {displayRightFormat(inputData.date1,'date2')}
                                </div>
                                <div className="svg2">
                                    <svg aria-hidden="true" class="bk-icon -streamline-arrow_nav_down sb-date-field__icon-arrow-down" fill="#333333" focusable="false" height="22" role="presentation" width="22" viewBox="0 0 24 24"><path d="M18 9.45c0 .2-.078.39-.22.53l-5 5a1.08 1.08 0 0 1-.78.32 1.1 1.1 0 0 1-.78-.32l-5-5a.75.75 0 0 1 0-1.06.74.74 0 0 1 1.06 0L12 13.64l4.72-4.72a.74.74 0 0 1 1.06 0 .73.73 0 0 1 .22.53zm-5.72 4.47zm-.57 0z"></path></svg>
                                </div>
                            </div>
                        </div>
                        {isComponentVisible && visible==='cal1' &&
                            <div className="date-calendar">
                                <Calendar
                                returnValue={"range"}
                                value={[inputData.date1,inputData.date2]}
                                defaultValue={[inputData.date1,inputData.date2]}
                                selectRange={true}
                                showDoubleView={true}
                                onChange={onChange}
                                showNeighboringMonth={false}
                                />
                            </div>
                        }
                        </div>
                        <Calendar2
                            onChange={onChange}
                            inputData={inputData}
                            setVisible={setVisible}
                            visible={visible}
                            displayRightFormat={displayRightFormat}
                        />
                        
        </div>
    )
}

export default CRCalendar