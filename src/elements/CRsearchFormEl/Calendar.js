import React,{useState,useEffect} from "react";
import useComponentVisible from "../../functions/UseComponenetVisibleHook";
import Calendar  from 'react-calendar'

const  CRCalendar = ({inputData,setInputData,handleChangeVal}) => {


    const { ref,isComponentVisible,setIsComponentVisible } = useComponentVisible(false);
    const [visible,setVisible] = useState(false)

    const onChange = (e) => {
        setInputData({
            ...inputData,
            date1: e[0],
            date2: e[1]
        })
        setVisible(false)
    }

    const weekDaysArr = ['pn','wt','śr','czw','pt','sob','nd']
    const monthArr = ['STY','LUT','MAR','KWI','MAJ','CZE','LIP','SIE','WRZ','PAŹ','LIS','GRU']

    const handleDisplayVal = (val) => {

            const day = val.getDay()
            const newDay = weekDaysArr[day]
            let monthDay = val.getUTCDate()
            const month = val.getMonth()
            const monthName = monthArr[month]
            let hour
            let min
            if(val===inputData.date1){
                hour = inputData.h1
                min = inputData.m1
                monthDay = monthDay+1
            }
            else if(val===inputData.date2){
                hour = inputData.h2
                min = inputData.m2
            }

            return newDay+" "+monthDay+" "+monthName+", "+hour+":"+min
    }
    



    return(
        <div ref={ref} className="date">
            <div  onClick={()=>{setIsComponentVisible(true);setVisible(true)}} className="date-wrapper">
                <div className="date-data">
                        <svg aria-hidden="true" class="bk-icon -experiments-calendar_checkin sb-date-picker_icon-svg" fill="#BDBDBD" focusable="false" height="24" role="presentation" width="24" viewBox="0 0 128 128"><path d="m112 16h-16v-8h-8v8h-48v-8h-8v8h-16c-4.4 0-8 3.9-8 8.7v86.6c0 4.8 3.6 8.7 8 8.7h96c4.4 0 8-3.9 8-8.7v-86.6c0-4.8-3.6-8.7-8-8.7zm0 95.3a1.1 1.1 0 0 1 -.2.7h-95.6a1.1 1.1 0 0 1 -.2-.7v-71.3h96zm-80-27.3h12v12h-12zm38-16h-12v-12h12zm0 28h-12v-12h12zm26 0h-12v-12h12zm0-28h-12v-12h12zm-48-16h-20v20h20zm-6 14h-8v-8h8z"></path></svg>
                        {handleDisplayVal(inputData.date1)}
                </div>
                <div className="date-data2">
                        <svg aria-hidden="true" class="bk-icon -experiments-calendar_checkin sb-date-picker_icon-svg" fill="#BDBDBD" focusable="false" height="24" role="presentation" width="24" viewBox="0 0 128 128"><path d="m112 16h-16v-8h-8v8h-48v-8h-8v8h-16c-4.4 0-8 3.9-8 8.7v86.6c0 4.8 3.6 8.7 8 8.7h96c4.4 0 8-3.9 8-8.7v-86.6c0-4.8-3.6-8.7-8-8.7zm0 95.3a1.1 1.1 0 0 1 -.2.7h-95.6a1.1 1.1 0 0 1 -.2-.7v-71.3h96zm-80-27.3h12v12h-12zm38-16h-12v-12h12zm0 28h-12v-12h12zm26 0h-12v-12h12zm0-28h-12v-12h12zm-48-16h-20v20h20zm-6 14h-8v-8h8z"></path></svg>
                        {handleDisplayVal(inputData.date2)}
                </div>
            </div>
            {isComponentVisible && visible &&
                <div className="date-calendar">
                            <Calendar
                                returnValue={"range"}
                                value={[inputData.date1,inputData.date2]}
                                selectRange={true}
                                showDoubleView={true}
                                onChange={onChange}
                            />

                    <div className="time-window">
                        <div className="window">
                            <div className="desc">Odbiór</div>
                            <div className="time-data">
                                <select 
                                value={inputData.h1}
                                name="h1"
                                onChange={(e)=> handleChangeVal(e)}>
                                    <option value="00">00</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                </select>
                            </div>
                            <div className="time-data">
                                <select
                                value={inputData.m1}
                                name="m1"
                                onChange={(e)=>handleChangeVal(e)}
                                >
                                    <option value="00">00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                </select>
                            </div>
                        </div>
                        <div className="window">
                            <div className="desc">Zwrot</div>
                            <div className="time-data">
                                <select
                                value={inputData.h2}
                                name="h2"
                                onChange={(e)=>handleChangeVal(e)}>
                                    <option value="01">01</option>
                                    <option value="00">00</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                </select>
                            </div>
                            <div className="time-data">
                                <select
                                value={inputData.m2}
                                name="m2"
                                onChange={(e)=>handleChangeVal(e)}>
                                    <option value="00">00</option>
                                    <option value="05">05</option>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="25">25</option>
                                    <option value="30">30</option>
                                    <option value="35">35</option>
                                    <option value="40">40</option>
                                    <option value="45">45</option>
                                    <option value="50">50</option>
                                    <option value="55">55</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}

export default CRCalendar
