import React,{useState} from "react"

const FilterSR = ({el,handleChecked,updatedHotelList}) => {

    const[checked, setChecked] = useState(false);

    const handleChange = (ch) => {
        setChecked(v => !v)
        handleChecked(ch)
    }

    const filterSP = el.sP
    const filterUP = el.uP

    const handleCheckboxDisplay = () => {
        if(el.uP){
            return "-"+el.uP+" zł"
        }
        else{
            return "+"
        }
    }

    const showLength = () =>{
        const checkLength = [...updatedHotelList].filter(el => el.price >= filterSP && el.price < filterUP);
        return checkLength.length
    }

    return(
        <div index={el.index} className="checkbox">
            <label class="container">
                <input value={checked} onChange={()=>handleChange(el.id)} type="checkbox"/>
                    <span class="checkmark"></span>
                    {el.sP} zł{handleCheckboxDisplay()}
                    
            </label>
            <span className="length">{showLength()}</span>
        </div>

    )
}
export default FilterSR