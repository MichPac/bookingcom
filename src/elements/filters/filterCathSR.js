import React,{useState} from "react"

const FilterCathSR = ({el,handleChecked,updatedHotelList}) => {

    const[checked, setChecked] = useState(false);

    const handleChange = () => {
        setChecked(v => !v)
        handleChecked(el.id)
    }

    const queryName = el.queryName

    const showLength = () =>{
        const checkLength = [...updatedHotelList].filter(el => el[queryName] === true)
        return checkLength.length
    }

    return(
        <div index={el.index} className="checkbox">
            <label class="container">
                <input value={checked} onChange={()=>handleChange(el.id)} type="checkbox"/>
                    <span class="checkmark"></span>
                    {el.name}
            </label>
        <span className="length">{showLength()}</span>
        </div>

    )
}
export default FilterCathSR