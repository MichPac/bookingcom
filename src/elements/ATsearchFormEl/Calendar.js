import React,{useState,useEffect} from "react";
import useComponentVisible from "../../functions/UseComponenetVisibleHook";
import Calendar  from 'react-calendar'

const  ATCalendar = ({setInputData,inputData}) => {



    const { ref,isComponentVisible,setIsComponentVisible } = useComponentVisible(false);

    const todayDate = new Date()

    const handleDateChange = (e) =>{
        setInputData({...inputData, date: e})
        setIsComponentVisible(false)
    }

    const getRightFormat = () => {
        let day = inputData.date.getUTCDate()+1
        let month = inputData.date.getMonth()
        let year = inputData.date.getFullYear()
        console.log(todayDate.getUTCDate())
        console.log(day)
        if(day>31){
            day = 1;
        }
        if(day<10){
            day = "0"+day
        }
        if(month<10){
            month = "0"+month
        }
        console.log(inputData.date)
        return (day+"/"+month+"/"+year)
    }
        
    return(
        <div className="date">
                                        <div ref={ref} className="date-wrapper">
                                        <div onClick={()=>setIsComponentVisible(true)} className="date-data">
                                            <svg  fill="grey" aria-hidden="true" class="bk-icon -experiments-calendar_checkin sb-date-picker_icon-svg" fill="grey"  focusable="false" height="24" role="presentation" width="24" viewBox="0 0 128 128"><path d="m112 16h-16v-8h-8v8h-48v-8h-8v8h-16c-4.4 0-8 3.9-8 8.7v86.6c0 4.8 3.6 8.7 8 8.7h96c4.4 0 8-3.9 8-8.7v-86.6c0-4.8-3.6-8.7-8-8.7zm0 95.3a1.1 1.1 0 0 1 -.2.7h-95.6a1.1 1.1 0 0 1 -.2-.7v-71.3h96zm-80-27.3h12v12h-12zm38-16h-12v-12h12zm0 28h-12v-12h12zm26 0h-12v-12h12zm0-28h-12v-12h12zm-48-16h-20v20h20zm-6 14h-8v-8h8z"></path></svg>
                                            {getRightFormat()}
                                        </div>
                                            {isComponentVisible && 

                                            <div className="calendar Calendar">
                                                    <Calendar
                                                        onChange={handleDateChange}
                                                        value={inputData.date}
                                                    />
                                            </div>
                                            }
                                        </div>
                                    </div>
    )
}

export default ATCalendar