import React,{useState} from "react"

const AccordionAT = (el) => {

    const[visible, setVisible] = useState(false);

    const toggleVisibility = () => {
        setVisible(v => !v);
    }

    return(
        <div onClick={toggleVisibility} index={el.id} className={` border-bottom ${visible ? "question-wrapper-active" : "question-wrapper"}`}>
            <div className="question-title">
            {el.question}
                <span>
                    <svg fill="#6b6b6b" class="bk-icon -iconset-navarrow_down bui-accordion__icon" height="30" role="presentation" width="30" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M64 88L33.2 57.2a4 4 0 0 1 5.6-5.7L64 76.7l25.2-25.2a4 4 0 0 1 5.6 5.7z"></path></svg>
                </span>
                </div>
                <div style={{display: visible ?  "unset" : "none" }}  className="question-answer">
                    <p>{el.answer}</p>
                </div>
            </div>

    )
}
export default AccordionAT 