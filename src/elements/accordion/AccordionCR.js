import React,{useState} from "react"

const AccordionCR = (el) => {

    const[visible, setVisible] = useState(false);

    const toggleVisibility = () => {
        setVisible(v => !v);
    }

    return(
        <div onClick={toggleVisibility} className={`border-bottom ${visible ? "question-wrapper" : "question-wrapper-active"}`}>
            <div className="question-title">{el.question}
                <span>
                    <svg fill="#6b6b6b" class="bk-icon -iconset-navarrow_down bui-accordion__icon" height="30" role="presentation" width="30" viewBox="0 0 128 128" aria-hidden="true" focusable="false"><path d="M64 88L33.2 57.2a4 4 0 0 1 5.6-5.7L64 76.7l25.2-25.2a4 4 0 0 1 5.6 5.7z"></path></svg>
                </span>
            </div>
            <div className="question-answer">
                {
                    el.p1 &&
                    <p>{el.p1}</p>
                }
                {
                    el.p2 &&
                    <p>{el.p2}</p>
                } 
                { el.l1 &&
                    <ul className="list">
                        <li>{el.l1}</li>
                        <li>{el.l2}</li>
                        <li>{el.l3}</li>
                        <li className={el.l4 ? '' : "displayNone"}>{el.l4}</li>
                    </ul>
                }
                {
                    el.p3 &&
                    <p>{el.p3}</p>
                } 
                {
                    el.p4 &&
                    <p>{el.p4}</p>
                } 
            </div>
        </div>

    )
}
export default AccordionCR